/*==========================================================================+
| write8svx.e                                                               |
| Effect class "write8svx", write to an IFF 8SVX sample file                |
+--------------------------------------------------------------------------*/

OPT MODULE, PREPROCESS

MODULE '*defs', '*file', '*iff8svx_ff', '*rnd', '*string', '*value',
       '*write', 'dos/dos'

/*-------------------------------------------------------------------------*/

EXPORT OBJECT write8svx OF write
PRIVATE
	vhdr : iff8svx_vhdr
	pos  : LONG         -> current sample position (starting at 0)
ENDOBJECT

PROC class() OF write8svx IS 'write8svx'

/*-------------------------------------------------------------------------*/

PROC new(list, name) OF write8svx
	SUPER self.new(list, name)
	self.vhdr.hioctsamples := 0         -> fixed in stopwrite
	self.vhdr.repeatstart  := 0
	self.vhdr.repeatlength := 0
	self.vhdr.rate         := 44100     -> fixed in stopwrite
	self.vhdr.octaves      := 1
	self.vhdr.compression  := 0
	self.vhdr.volume       := 65536
ENDPROC

/*-------------------------------------------------------------------------*/

PROC startwrite() OF write8svx
	DEF fh
	IF fh := self._fh()
		write_ulong(fh, MAGIC_FORM)
		write_ulong(fh, 0)          -> fixed in stopwrite
		write_ulong(fh, MAGIC_8SVX)
		write_ulong(fh, MAGIC_VHDR)
		write_ulong(fh, SIZEOF iff8svx_vhdr)
		IF write(fh, self.vhdr, SIZEOF iff8svx_vhdr) = FALSE
			Throw("file", fh)
		ENDIF
		write_ulong(fh, MAGIC_BODY)
		write_ulong(fh, 0)          -> fixed in stopwrite
	ENDIF
	self.pos := 0
ENDPROC

/*-------------------------------------------------------------------------*/

PROC stopwrite() OF write8svx
	DEF fh
	IF fh := self._fh()
		Seek(fh,  4, OFFSET_BEGINNING)      -> FORM.length
		write_ulong(fh, 20 + SIZEOF iff8svx_vhdr + self.pos)
		Seek(fh, 20, OFFSET_BEGINNING)      -> VHDR.hisamples
		write_ulong(fh, self.pos)
		Seek(fh, 24 + SIZEOF iff8svx_vhdr, OFFSET_BEGINNING)
		write_ulong(fh, self.pos)           -> BODY.length
	ENDIF
ENDPROC

/*-------------------------------------------------------------------------*/

PROC process() OF write8svx
	DEF smp
	SUPER self.process()
	smp := ! self._main() * 128.0 !
	IF self._fh() THEN write_sbyte(self._fh(), Bounds(smp, -128, 127))
	self.pos := self.pos + 1
ENDPROC

/*--------------------------------------------------------------------------+
| END: write8svx.e                                                          |
+==========================================================================*/
