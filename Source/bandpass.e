/*==========================================================================+
| bandpass.e                                                                |
| Effect class "bandpass", band pass filter                                 |
+--------------------------------------------------------------------------*/

OPT MODULE, PREPROCESS

MODULE '*defs', '*bandfilter', '*link'

EXPORT OBJECT bandpass OF bandfilter
ENDOBJECT

PROC class() OF bandpass IS 'bandpass'

PROC new(list, name) OF bandpass
	SUPER self.new(list, name)
	self.setrecalc()
	self.reset()
ENDPROC

PROC recalc() OF bandpass
	DEF c, d, in : PTR TO link
	SUPER self.recalc()
	in := self.getinput(ID_MAIN)
	c := ! 1.0 / Ftan(! PI  * self.band / in.rate)
	d := ! 2.0 * Fcos(! PI2 * self.freq / in.rate)
	self.a0 := ! 1.0 / (! 1.0 + c)
	self.a1 := 0.0
	self.a2 := ! -self.a0
	self.b1 := ! c * d * self.a0
	self.b2 := ! (! 1.0 - c) * self.a0
ENDPROC

/*--------------------------------------------------------------------------+
| END: bandpass.e                                                           |
+==========================================================================*/
