/*==========================================================================+
| debug.e                                                                   |
| Debugging macros                                                          |
|                                                                           |
| DEBUG             this is defined to enable debugging, as a suitable      |
|                   prefix for printing debug info (use                     |
|                   "PrintF(DEBUG 'string',args)")                          |
| debug(x)          this expands to x when debugging is enabled, otherwise  |
|                   to nothing, neater than #ifdef ... #endif               |
| ASSERT            defined to enable assertion                             |
| assert(x, str)    when assertion is enabled, throws an "asrt" exception   |
|                   with str as info if x is false                          |
|                                                                           |
+--------------------------------------------------------------------------*/

OPT MODULE, PREPROCESS
OPT EXPORT

/*-------------------------------------------------------------------------*/

-> Comment [uncomment] next lines to remove [insert] debugging code
->#define DEBUG '++ Debug: '+
->#define ASSERT

/*-------------------------------------------------------------------------*/

#ifdef DEBUG
#define debug(x) x
#define realf(x) RealF(String(16), x, 8)
#endif

#ifndef DEBUG
#define debug(x)
#endif

/*-------------------------------------------------------------------------*/

#ifdef ASSERT
#define assert(x,str) IF (x) = FALSE THEN Throw("asrt", str)
#endif

#ifndef ASSERT
#define assert(x,str)
#endif

/*--------------------------------------------------------------------------+
| END: debug.e                                                              |
+==========================================================================*/
