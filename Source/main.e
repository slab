/*==========================================================================+
| main.e                                                                    |
| System entry point                                                        |
|                                                                           |
| E automatically opens exec.library, dos.library, mathieeesingbas.library  |
| and mathieeesingtrans.library                                             |
|                                                                           |
+--------------------------------------------------------------------------*/

OPT PREPROCESS

/*-------------------------------------------------------------------------*/

MODULE '*cli', '*rnd', '*string'

/*-------------------------------------------------------------------------*/

-> workaround for not being able to use EXIT in LOOP
#define _EXIT(x) IF (x) THEN JUMP _exit_endloop

PROC main() HANDLE

	DEF cli = NIL : PTR TO cli, instr = NIL : PTR TO CHAR, allocout = FALSE

	-> Initialise
	IF stdout = NIL     -> not started from system CLI (eg, from Workbench)
		stdout := Open('KCON:////SLab/CLOSE/WAIT', NEWFILE)
		IF stdout = NIL THEN CleanUp(20)    -> DOS return code FAIL
		allocout := TRUE                    -> don't close if not ours
	ENDIF
	initseed($AF7642B9)     -> random number seed
	NEW cli.new()

	-> Main loop
	instr := String(1024)   -> allow long input
	LOOP
		PrintF('>> ')       -> prompt
		IF Fgets(stdout, instr, 1023) -> bug in OS, sometimes writes past end
			_EXIT( cli.parse(instr) )
		ELSE
			-> EOF, ie close window or control-\
			PrintF('\n')    -> no return entered by user, neat output
			_EXIT( TRUE )
		ENDIF
	ENDLOOP
_exit_endloop:

	PrintF('** Bye!\n')     -> exit message

EXCEPT DO

	-> Clean up
	IF cli THEN END cli
	IF allocout
		Close(stdout)   -> will be open, CleanUp()ed above if not
		stdout := NIL
	ENDIF

	-> Report fatal errors
	SELECT exception
	CASE 0;     RETURN 0    -> no error
	CASE "MEM"; PrintF('** Error: out of memory\n')
	CASE "NEW"; PrintF('** Error: out of memory\n')
	-> Should never be seen
	CASE "asrt"
		PrintF('** Error: assertion failed in "\s"\n', exceptioninfo)
	DEFAULT
		PrintF('** Error: \z\h[8] \z\h[8]\n', exception, exceptioninfo)
	ENDSELECT

ENDPROC	5   -> DOS return code WARN

/*--------------------------------------------------------------------------+
| END: main.e                                                               |
+==========================================================================*/
