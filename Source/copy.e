/*==========================================================================+
| copy.e                                                                    |
| Effect class "copy", copies input to output                               |
+--------------------------------------------------------------------------*/

OPT MODULE

MODULE '*defs', '*in1out1'

EXPORT OBJECT copy OF in1out1
ENDOBJECT

PROC class() OF copy IS 'copy'

PROC process() OF copy
	SUPER self.process()
	self.output(ID_MAIN, self._main())
ENDPROC

/*--------------------------------------------------------------------------+
| END: copy.e                                                               |
+==========================================================================*/
