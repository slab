/*==========================================================================+
| sine.e                                                                    |
| Effect class "sine", sine oscillator                                      |
+--------------------------------------------------------------------------*/

OPT MODULE, PREPROCESS

MODULE '*defs', '*osc'

EXPORT OBJECT sine OF osc
ENDOBJECT

PROC oscillator(time) OF sine IS Fsin(! PI2 * time)  -> 2 PI * time

PROC class() OF sine IS 'sine'

/*--------------------------------------------------------------------------+
| END: sine.e                                                               |
+==========================================================================*/
