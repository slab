/*==========================================================================+
| bandreject.e                                                              |
| Effect class "bandreject", band reject filter                             |
+--------------------------------------------------------------------------*/

OPT MODULE, PREPROCESS

MODULE '*defs', '*bandfilter', '*link'

EXPORT OBJECT bandreject OF bandfilter
ENDOBJECT

PROC class() OF bandreject IS 'bandreject'

PROC new(list, name) OF bandreject
	SUPER self.new(list, name)
	self.setrecalc()
	self.reset()
ENDPROC

PROC recalc() OF bandreject
	DEF c, d, in : PTR TO link
	SUPER self.recalc()
	in := self.getinput(ID_MAIN)
	c := ! Ftan(! PI  * self.band / in.rate)
	d := ! 2.0 * Fcos(! PI2 * self.freq / in.rate)
	self.a0 := ! 1.0 / (! 1.0 + c)
	self.a1 := ! -d * self.a0
	self.a2 := ! self.a0
	self.b1 := ! -self.a1
	self.b2 := ! (! c - 1.0) * self.a0
ENDPROC

/*--------------------------------------------------------------------------+
| END: bandreject.e                                                         |
+==========================================================================*/
