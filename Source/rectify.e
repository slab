/*==========================================================================+
| rectify.e                                                                 |
| Effect class "rectify", rectifies input                                   |
+--------------------------------------------------------------------------*/

OPT MODULE

MODULE '*defs', '*in1out1'

EXPORT OBJECT rectify OF in1out1
ENDOBJECT

PROC class() OF rectify IS 'rectify'

PROC process() OF rectify
	SUPER self.process()
	self.output(ID_MAIN, Fabs(self._main()))
ENDPROC

/*--------------------------------------------------------------------------+
| END: rectify.e                                                            |
+==========================================================================*/
