/*==========================================================================+
| in1out1.e                                                                 |
| Effect base class in1out1                                                 |
| Defines an input "main" and an output "main"                              |
| in1out1._main()       get input sample (for efficiency in process)        |
+--------------------------------------------------------------------------*/

OPT MODULE, PREPROCESS

MODULE '*effect', '*defs', '*link', '*string', '*debug'

/*-------------------------------------------------------------------------*/

EXPORT OBJECT in1out1 OF effect
PRIVATE
	in  : link
	out : PTR TO link
ENDOBJECT

PROC class() OF in1out1 IS 'in1out1'
PROC _main() OF in1out1 IS self.in.data

/*-------------------------------------------------------------------------*/

PROC new(list, name) OF in1out1
	SUPER self.new(list, name)
	newlink(self.in, self, ID_MAIN)
	self.out := NIL
ENDPROC

/*-------------------------------------------------------------------------*/

PROC end() OF in1out1
	unlink(self.in)
	unlink(self.out)
	self.out := NIL
ENDPROC SUPER self.end()

/*-------------------------------------------------------------------------*/

PROC input2id (str)     OF in1out1
ENDPROC IF strcmp(IDS_MAIN, str) THEN ID_MAIN ELSE SUPER self.input2id(str)

PROC output2id(str)     OF in1out1
ENDPROC IF strcmp(IDS_MAIN, str) THEN ID_MAIN ELSE SUPER self.output2id(str)

PROC id2input (id)      OF in1out1
ENDPROC IF id = ID_MAIN THEN IDS_MAIN ELSE SUPER self.id2input(id)

PROC id2output(id)      OF in1out1
ENDPROC IF id = ID_MAIN THEN IDS_MAIN ELSE SUPER self.id2output(id)

PROC getinput(id)       OF in1out1
ENDPROC IF id = ID_MAIN THEN self.in ELSE SUPER self.getinput(id)

PROC isready()          OF in1out1
ENDPROC IF self.in.ready THEN SUPER self.isready() ELSE FALSE

/*-------------------------------------------------------------------------*/

PROC setoutput(id, link)    OF in1out1
	SELECT id
	CASE ID_MAIN;   self.out := link
	DEFAULT;        SUPER self.setoutput(id, link)
	ENDSELECT
ENDPROC

/*-------------------------------------------------------------------------*/

PROC clear()            OF in1out1
	clearlink(self.in)
ENDPROC SUPER self.clear()

/*-------------------------------------------------------------------------*/

PROC output(id, data)   OF in1out1
	SELECT id
	CASE ID_MAIN;   outputlink(self.out, data)
	DEFAULT;        SUPER self.output(id, data)
	ENDSELECT
ENDPROC

/*-------------------------------------------------------------------------*/

PROC check() OF in1out1
	IF SUPER self.check() = FALSE THEN RETURN FALSE
	IF self.in.from = NIL
		Throw(ERR_CHECK, [ CHECK_INPUT_NOT_CONNECTED, self, ID_MAIN ])
	ENDIF
	IF self.out = NIL
		Throw(ERR_CHECK, [ CHECK_OUTPUT_NOT_CONNECTED, self, ID_MAIN ])
	ENDIF
	self.out.rate := self.in.rate
ENDPROC self.out.to.check()

/*--------------------------------------------------------------------------+
| END: in1out1.e                                                            |
+==========================================================================*/
