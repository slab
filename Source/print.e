/*==========================================================================+
| print.e                                                                   |
| Effect class "print"                                                      |
| Used in debugging only, as it prints the object name and the input to the |
| screen each time process is called.                                       |
+--------------------------------------------------------------------------*/

OPT MODULE
OPT EXPORT

MODULE '*defs', '*in1out0', '*list'

RAISE "MEM" IF String() = NIL

/*-------------------------------------------------------------------------*/

OBJECT print OF in1out0
PRIVATE
	buffer  : PTR TO CHAR       -> E-String
ENDOBJECT

PROC class() OF print IS 'print'

/*-------------------------------------------------------------------------*/

PROC new(list, name) OF print
	SUPER self.new(list, name)
	self.buffer := String(32)
ENDPROC

/*-------------------------------------------------------------------------*/

PROC end() OF print
	DisposeLink(self.buffer)
	self.buffer := NIL
ENDPROC SUPER self.end()

/*-------------------------------------------------------------------------*/

PROC process() OF print
	DEF n : PTR TO node
	SUPER self.process()
	n := self.node
	PrintF('\s \s\n', n.name, RealF(self.buffer, self._main(), 8))
ENDPROC

/*--------------------------------------------------------------------------+
| END: print.e                                                              |
+==========================================================================*/
