/*==========================================================================+
| halfrectify.e                                                             |
| Effect class "halfrectify", half rectifies input                          |
+--------------------------------------------------------------------------*/

OPT MODULE

MODULE '*defs', '*in1out1'

EXPORT OBJECT halfrectify OF in1out1
ENDOBJECT

PROC class() OF halfrectify IS 'halfrectify'

PROC process() OF halfrectify
	SUPER self.process()
	self.output(ID_MAIN, IF ! self._main() > 0.0 THEN self._main() ELSE 0.0)
ENDPROC

/*--------------------------------------------------------------------------+
| END: halfrectify.e                                                        |
+==========================================================================*/
