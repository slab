/*==========================================================================+
| classnode.e                                                               |
| Node structure for list of classes                                        |
|                                                                           |
| classnode.newf    address of a function with arguments "list, name" that  |
|                   returns a new object of that class, linked into list    |
+--------------------------------------------------------------------------*/

OPT MODULE

MODULE '*list'

EXPORT OBJECT classnode OF node
	newf    : LONG
ENDOBJECT

/*
	classnode.newf := {eg}
	PROC eg(list, name, o = NIL : PTR TO eg) IS NEW o.new(list, name)
*/

/*--------------------------------------------------------------------------+
| END: classnode.e                                                          |
+==========================================================================*/
