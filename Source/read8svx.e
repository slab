/*==========================================================================+
| read8svx.e                                                                |
| Effect class "read8svx", read from an IFF 8SVX sample file                |
+--------------------------------------------------------------------------*/

OPT MODULE, PREPROCESS

MODULE '*defs', '*file', '*read', '*iff8svx_ff', '*string', '*value',
       '*debug', 'dos/dos'

/*-------------------------------------------------------------------------*/

EXPORT OBJECT read8svx OF read
PRIVATE
	vhdr : iff8svx_vhdr -> data
	len  : LONG         -> number of samples
	pos  : LONG         -> current sample position (starting at 0)
ENDOBJECT

PROC class() OF read8svx IS 'read8svx'

/*-------------------------------------------------------------------------*/

PROC new(list, name) OF read8svx
	SUPER self.new(list, name)
ENDPROC

/*-------------------------------------------------------------------------*/

-> Workaround for deficiency in E
#define _EXIT(x) IF (x) THEN JUMP _exit_endloop

PROC startread() OF read8svx
	DEF fh, id, len, gotvhdr = FALSE
	IF fh := self._fh()
		IF read_ulong(fh) <> MAGIC_FORM THEN Throw(ERR_BAD_FILE_FORMAT, fh)
		read_ulong(fh)  -> file length - 4
		IF read_ulong(fh) <> MAGIC_8SVX THEN Throw(ERR_BAD_FILE_FORMAT, fh)
		LOOP    -> up to BODY chunk
			id  := read_ulong(fh)   -> Exception exits loop if EOF
			len := read_ulong(fh)
			SELECT id
			CASE MAGIC_VHDR         -> voice header
				gotvhdr := TRUE
				IF len <> SIZEOF iff8svx_vhdr
					Throw(ERR_BAD_FILE_FORMAT, fh)
				ENDIF
				-> len = SIZEOF iff8svx_vhdr
				IF read(fh, self.vhdr, len) = FALSE
					Throw(ERR_BAD_FILE_FORMAT, fh)
				ENDIF
				-> check parameters
				IF (self.vhdr.octaves <> 1) OR      -> no multioctave
				   (self.vhdr.compression <> 0)     -> no compression
					Throw(ERR_BAD_FILE_FORMAT, fh)
				ENDIF
			CASE MAGIC_BODY
				IF gotvhdr = FALSE THEN Throw(ERR_BAD_FILE_FORMAT, fh)
				-> At start of sample data
				self.len := len
				_EXIT(TRUE)
			DEFAULT
				-> Skip unknown chunk
				Seek(fh, len, OFFSET_CURRENT)
			ENDSELECT
		ENDLOOP
_exit_endloop:
	ENDIF
	self.pos := 0
ENDPROC

/*-------------------------------------------------------------------------*/

PROC process() OF read8svx
	DEF smp = 0.0
	SUPER self.process()
	IF (self.pos < self.len) AND self._fh()
		smp := read_sbyte(self._fh()) ! / 128.0
	ENDIF
	self.pos := self.pos + 1
ENDPROC self.output(ID_MAIN, smp)

/*--------------------------------------------------------------------------+
| END: read8svx.e                                                           |
+==========================================================================*/
