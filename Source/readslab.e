/*==========================================================================+
| readslab.e                                                                |
| Effect class "readslab"                                                   |
| Read from a SLab sample file (see slab.txt)                               |                                                                           |
| Parameter "normalize" = "on" / "off" normalizes output                    |
+--------------------------------------------------------------------------*/

OPT MODULE, PREPROCESS
OPT EXPORT

MODULE '*defs', '*file', '*read', '*slab_ff', '*string', '*value', '*debug'

/*-------------------------------------------------------------------------*/

OBJECT readslab OF read
PRIVATE
	info : slab_info    -> data
	norm : LONG         -> normalize?
	len  : LONG         -> number of samples
	pos  : LONG         -> current sample position (starting at 0)
ENDOBJECT

PROC class() OF readslab IS 'readslab'

/*-------------------------------------------------------------------------*/

PROC new(list, name) OF readslab
	SUPER self.new(list, name)
	self.norm := TRUE
ENDPROC

/*-------------------------------------------------------------------------*/

PROC startread() OF readslab
	DEF fh
	IF fh := self._fh()
		IF read_ulong(fh) <> MAGIC_SLab THEN Throw(ERR_BAD_FILE_FORMAT, fh)
		read_ulong(fh)  -> file length - 4
		IF read_ulong(fh) <> MAGIC_Info THEN Throw(ERR_BAD_FILE_FORMAT, fh)
		IF read_ulong(fh) <> 12         THEN Throw(ERR_BAD_FILE_FORMAT, fh)
		self.info.rate := read_ulong(fh)
		self.info.bias := read_ulong(fh)
		self.info.ampl := read_ulong(fh)
		IF read_ulong(fh) <> MAGIC_Data THEN Throw(ERR_BAD_FILE_FORMAT, fh)
		self.len := Div(read_ulong(fh), 4)   -> standard * / are only 16 bit
	ENDIF
	self.pos := 0
ENDPROC

/*-------------------------------------------------------------------------*/

PROC param2id(str) OF readslab
	IF strcmp(IDS_NORMALIZE, str); RETURN ID_NORMALIZE
	ENDIF
ENDPROC SUPER self.param2id(str)

/*-------------------------------------------------------------------------*/

PROC id2param(id) OF readslab
	SELECT id
	CASE ID_NORMALIZE; RETURN IDS_NORMALIZE
	ENDSELECT
ENDPROC SUPER self.id2param(id)

/*-------------------------------------------------------------------------*/

PROC paramtype(id) OF readslab
	SELECT id
	CASE ID_NORMALIZE; RETURN TYPE_STRING
	ENDSELECT
ENDPROC SUPER self.paramtype(id)

/*-------------------------------------------------------------------------*/

PROC set(id, data) OF readslab
	SELECT id
	CASE ID_NORMALIZE
		IF strcmp(IDS_ON, data)
			self.norm := TRUE
		ELSEIF strcmp(IDS_OFF, data)
			self.norm := FALSE
		ELSE
			Throw(ERR_BAD_RANGE, data)
		ENDIF
		RETURN TRUE
	ENDSELECT
ENDPROC SUPER self.set(id, data)

/*-------------------------------------------------------------------------*/

PROC get(id) OF readslab
	SELECT id
	CASE ID_NORMALIZE; RETURN IF self.norm THEN IDS_ON ELSE IDS_OFF
	ENDSELECT
ENDPROC SUPER self.get(id)

/*-------------------------------------------------------------------------*/

PROC process() OF readslab
	DEF smp = 0.0
	SUPER self.process()
	IF (self.pos < self.len) AND self._fh()
		smp := read_ulong(self._fh())
		IF self.norm THEN smp := ! (! smp - self.info.bias) / self.info.ampl
	ENDIF
	self.pos := self.pos + 1
ENDPROC self.output(ID_MAIN, smp)

/*--------------------------------------------------------------------------+
| END: readslab.e                                                           |
+==========================================================================*/
