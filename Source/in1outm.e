/*==========================================================================+
| in1outm.e                                                                 |
| Effect base class in1outm                                                 |
|                                                                           |
| Multiple outputs, single input.  The number of outputs can be set once,   |
| after which attempts to change it fail.                                   |
|                                                                           |
| Defines an input "main" and outputs "outX", X = 1, 2, ... "outputs" param |
|                                                                           |
| in1outm._outputs()     get number of inputs (for efficiency in process)   |
| in1outm._out(x)        get output id                                      |
| in1outm._in(x)         get input sample (for efficiency in process)       |
|                                                                           |
+--------------------------------------------------------------------------*/

OPT MODULE, PREPROCESS

MODULE '*effect', '*defs', '*link', '*string', '*value', '*debug'

RAISE "MEM" IF New() = NIL

/*-------------------------------------------------------------------------*/

EXPORT OBJECT in1outm OF effect
PRIVATE
	outs : LONG
	out  : PTR TO LONG   -> ptr to ptr to link
	in   : link
ENDOBJECT

PROC class()    OF in1outm IS 'in1outm'
PROC _outputs() OF in1outm IS self.outs
PROC _out(x)    OF in1outm IS ID_MULTI_OUT + x
PROC _in()      OF in1outm IS self.in.data

/*-------------------------------------------------------------------------*/

PROC new(list, name) OF in1outm
	SUPER self.new(list, name)
	self.outs := 0
	self.out  := NIL
	newlink(self.in, self, ID_MAIN)
ENDPROC

/*-------------------------------------------------------------------------*/

PROC end() OF in1outm
	DEF i
	IF self.out
		FOR i := 0 TO self.outs - 1
			IF self.out[i] THEN unlink(self.out[i])
		ENDFOR
		Dispose(self.out)
		self.out := NIL
	ENDIF
	unlink(self.in)
ENDPROC SUPER self.end()

/*-------------------------------------------------------------------------*/

PROC input2id (str)     OF in1outm
ENDPROC IF strcmp(IDS_MAIN, str) THEN ID_MAIN ELSE SUPER self.input2id(str)

PROC id2input (id)      OF in1outm
ENDPROC IF id = ID_MAIN THEN IDS_MAIN ELSE SUPER self.id2input(id)

PROC getinput(id)       OF in1outm
ENDPROC IF id = ID_MAIN THEN self.in ELSE SUPER self.getinput(id)

PROC get(id) OF in1outm
ENDPROC IF id = ID_OUTPUTS THEN self.outs ELSE SUPER self.get(id)

PROC isready()          OF in1outm
ENDPROC IF self.in.ready THEN SUPER self.isready() ELSE FALSE

/*-------------------------------------------------------------------------*/

PROC output2id(str) OF in1outm
	DEF id, len
	IF strncmp(IDS_OUT, str, 3)
		id, len := Val(str + 3)
		IF ((len + 3) = StrLen(str)) AND (0 < id) AND (id <= self.outs)
			RETURN ID_MULTI_OUT + id
		ENDIF
	ENDIF
ENDPROC SUPER self.output2id(str)

/*-------------------------------------------------------------------------*/

PROC param2id(str) OF in1outm
	IF strcmp(IDS_OUTPUTS, str); RETURN ID_OUTPUTS
	ENDIF
ENDPROC SUPER self.param2id(str)

/*-------------------------------------------------------------------------*/

PROC id2output(id)      OF in1outm
	DEF outid
	outid := id - ID_MULTI_OUT
	IF (0 < outid) AND (outid <= self.outs)
		RETURN StringF(String(8), 'out\d', outid)
	ENDIF
ENDPROC SUPER self.id2output(id)

/*-------------------------------------------------------------------------*/

PROC id2param(id) OF in1outm
	SELECT id
	CASE ID_OUTPUTS; RETURN IDS_OUTPUTS
	ENDSELECT
ENDPROC SUPER self.id2param(id)

/*-------------------------------------------------------------------------*/

PROC paramtype(id) OF in1outm
	SELECT id
	CASE ID_OUTPUTS; RETURN TYPE_NUMBER
	ENDSELECT
ENDPROC SUPER self.paramtype(id)

/*-------------------------------------------------------------------------*/

PROC set(id, data) OF in1outm
	DEF i
	SELECT id
	CASE ID_OUTPUTS
		IF ! data ! <= 0 THEN Throw(ERR_BAD_RANGE, id)
		-> Cannot change number of outputs after first set (requires either
		-> relinking of all existing links or keeping all existing links and
		-> adding)
		IF self.out THEN RETURN FALSE
		self.out := New(! data ! * SIZEOF LONG)
		self.outs := ! data !
		-> Clear links
		FOR i := 0 TO self.outs - 1
			self.out[i] := NIL
		ENDFOR
		RETURN TRUE
	ENDSELECT
ENDPROC SUPER self.set(id, data)

/*-------------------------------------------------------------------------*/

PROC setoutput(id, link)    OF in1outm
	DEF outid
	outid := id - ID_MULTI_OUT
	IF (0 < outid) AND (outid <= self.outs)
		self.out[outid - 1] := link
		RETURN TRUE
	ENDIF
ENDPROC SUPER self.setoutput(id, link)

/*-------------------------------------------------------------------------*/

PROC clear() OF in1outm
	clearlink(self.in)
ENDPROC SUPER self.clear()

/*-------------------------------------------------------------------------*/

PROC output(id, data)   OF in1outm
	DEF outid
	outid := id - ID_MULTI_OUT
	IF (0 < outid) AND (outid <= self.outs)
		RETURN outputlink(self.out[outid - 1], data)
	ENDIF
ENDPROC SUPER self.output(id, data)

/*-------------------------------------------------------------------------*/

PROC check() OF in1outm
	DEF i, ok, l : PTR TO link
	IF SUPER self.check() = FALSE THEN RETURN FALSE
	IF self.in.from = NIL
		Throw(ERR_CHECK, [ CHECK_INPUT_NOT_CONNECTED, self, ID_MAIN ])
	ENDIF
	IF self.out = NIL
		Throw(ERR_CHECK, [ CHECK_INTERNAL_ERROR, self, 'no outputs' ])
	ENDIF
	FOR i := 0 TO self.outs - 1
		l := self.out[i]
		IF l = NIL
			Throw(ERR_CHECK,
			     [ CHECK_OUTPUT_NOT_CONNECTED, self, i + 1 + ID_MULTI_OUT])
		ENDIF
		l.rate := self.in.rate
	ENDFOR
	ok := TRUE
	FOR i := 0 TO self.outs - 1
		l := self.out[i]
		ok := ok AND l.to.check()
	ENDFOR
ENDPROC ok

/*--------------------------------------------------------------------------+
| END: in1outm.e                                                            |
+==========================================================================*/
