/*==========================================================================+
| delay.e                                                                   |
| Effect class "delay", delays input by parameter "delay"                   |
+--------------------------------------------------------------------------*/

OPT MODULE, PREPROCESS

MODULE '*cbuffer', '*defs', '*in1out1', '*link', '*string', '*value',
       '*debug'

/*-------------------------------------------------------------------------*/

EXPORT OBJECT delay OF in1out1
PUBLIC      -> fbdelay needs access
	cb  : PTR TO cbuffer
	dt  : LONG
ENDOBJECT

PROC class() OF delay IS 'delay'

/*-------------------------------------------------------------------------*/

PROC new(list, name) OF delay
	SUPER self.new(list, name)
	self.cb := NIL      -> deferred recalc() => cbuffer not allocated now
	self.set(ID_DELAY, 0.05)
ENDPROC

/*-------------------------------------------------------------------------*/

PROC end() OF delay
	END self.cb
ENDPROC SUPER self.end()

/*-------------------------------------------------------------------------*/

PROC param2id(str) OF delay
ENDPROC IF strcmp(IDS_DELAY, str) THEN ID_DELAY ELSE SUPER self.param2id(str)

PROC id2param(id) OF delay
ENDPROC IF id = ID_DELAY THEN IDS_DELAY ELSE SUPER self.id2param(id)

PROC paramtype(id) OF delay
ENDPROC IF id = ID_DELAY THEN TYPE_NUMBER ELSE SUPER self.paramtype(id)

PROC get(id) OF delay
ENDPROC IF id = ID_DELAY THEN self.dt ELSE SUPER self.get(id)

/*-------------------------------------------------------------------------*/

PROC set(id, data) OF delay
	SELECT id
	CASE ID_DELAY
	    self.dt := data
	    self.setrecalc()
	DEFAULT; SUPER self.set(id, data)
	ENDSELECT
ENDPROC

/*-------------------------------------------------------------------------*/

PROC recalc() OF delay
	DEF in  : PTR TO link
	SUPER self.recalc()
	in := self.getinput(ID_MAIN)
	IF self.cb
		self.cb.setlength(! self.dt * in.rate)
	ELSE
		NEW self.cb.new(! self.dt * in.rate)
	ENDIF
ENDPROC

/*-------------------------------------------------------------------------*/

PROC reset() OF delay
	SUPER self.reset()
	self.cb.clear()     -> reset buffer to 0
ENDPROC

/*-------------------------------------------------------------------------*/

PROC process() OF delay
	SUPER self.process()
	self.cb.next()
	self.cb.write(self._main())
	self.output(ID_MAIN, self.cb.read())
ENDPROC

/*--------------------------------------------------------------------------+
| END: delay.e                                                              |
+==========================================================================*/
