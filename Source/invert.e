/*==========================================================================+
| invert.e                                                                  |
| Effect class "invert", inverts input                                      |
+--------------------------------------------------------------------------*/

OPT MODULE

MODULE '*defs', '*in1out1'

EXPORT OBJECT invert OF in1out1
ENDOBJECT

PROC class() OF invert IS 'invert'

PROC process() OF invert
	SUPER self.process()
	self.output(ID_MAIN, ! - self._main())
ENDPROC

/*--------------------------------------------------------------------------+
| END: invert.e                                                             |
+==========================================================================*/
