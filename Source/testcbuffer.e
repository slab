/*==========================================================================+
| testcbuffer.e                                                             |
| Test circular buffer class                                                |
+--------------------------------------------------------------------------*/

MODULE '*cbuffer'

ENUM ERR_OK = 0, ERR_MATHLIB

RAISE "MEM" IF String() = NIL

PROC main() HANDLE

	DEF a   = NIL   : PTR TO cbuffer,
	    b   = NIL   : PTR TO cbuffer,
	    c   = NIL   : PTR TO cbuffer,
	    in  = 0.0   : LONG,
	    i   = 0     : LONG,
	    bin = NIL   : PTR TO CHAR,      -> estring buffers for RealF()
	    ba  = NIL   : PTR TO CHAR,
	    bb  = NIL   : PTR TO CHAR,
	    bc  = NIL   : PTR TO CHAR,
	    bd  = NIL   : PTR TO CHAR

    NEW a.new(5.0)      -> check integer length
    NEW b.new(5.5)      -> check fractional length
    NEW c.new(1.0)      -> check short length

	bin := String(16)
	ba  := String(16)
	bb  := String(16)
	bc  := String(16)
	bd  := String(16)

	PrintF('i\tin\ta\tb\tc\ta(-3.2)\n')

	FOR i := 1 TO 80
		IF i = 10 THEN c.setlength(50.0)    -> check reallocation
		IF i = 20 THEN a.setlength(8.0)     -> check increase
		IF i = 40 THEN b.setlength(3.0)     -> check decrease
		IF i = 50 THEN a.clear()            -> check clear
		IF i = 60 THEN c.setlength(0.0)     -> check zero length
		in := i ! * .1
		a.write(in)                         -> check write
		b.write(in)
		c.write(in)
		RealF(bin, in, 4)
		RealF(ba, a.read(), 4)              -> check read
		RealF(bb, b.read(), 4)
		RealF(bc, c.read(), 4)
		RealF(bd, a.readrel(-3.2), 4)       -> check relative read
		PrintF('\d[2]\t\s\t\s\t\s\t\s\t\s\n', i, bin, ba, bb, bc, bd)
		a.next()                            -> check next
		b.next()
		c.next()
	ENDFOR

EXCEPT DO

	IF bc  THEN Dispose(bc)
	IF bb  THEN Dispose(bb)
	IF ba  THEN Dispose(ba)
	IF bin THEN Dispose(bin)

	IF c THEN END c
	IF b THEN END b
	IF a THEN END a

	SELECT exception
	CASE ERR_OK
		RETURN 0
	CASE ERR_MATHLIB
		PrintF('** Error: couldn''t open "mathieeesingbas.library"\n')
	CASE "asrt"
		PrintF('** Error: assertion failed in "\s"\n', exceptioninfo)
	DEFAULT
		PrintF('** Unknown error: \z\h[8] \z\h[8]\n',
		                    exception, exceptioninfo)
	ENDSELECT

ENDPROC 5

/*--------------------------------------------------------------------------+
| END: testcbuffer.e                                                        |
+==========================================================================*/
