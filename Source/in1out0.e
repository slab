/*==========================================================================+
| in1out0.e                                                                 |
| Effect base class in1out0                                                 |
| Defines an input "main"                                                   |
| in1out0._main()       get input sample (for efficiency in process)        |
+--------------------------------------------------------------------------*/

OPT MODULE, PREPROCESS

MODULE '*effect', '*defs', '*link', '*string'

/*-------------------------------------------------------------------------*/

EXPORT OBJECT in1out0 OF effect
PRIVATE
	in  : link
ENDOBJECT

PROC class() OF in1out0 IS 'in1out0'
PROC _main() OF in1out0 IS self.in.data

/*-------------------------------------------------------------------------*/

PROC new(list, name) OF in1out0
	SUPER self.new(list, name)
	newlink(self.in, self, ID_MAIN)
ENDPROC

/*-------------------------------------------------------------------------*/

PROC end() OF in1out0
	unlink(self.in)
ENDPROC SUPER self.end()

/*-------------------------------------------------------------------------*/

PROC input2id (str) OF in1out0
ENDPROC IF strcmp(IDS_MAIN, str) THEN ID_MAIN ELSE SUPER self.input2id(str)

PROC id2input (id) OF in1out0
ENDPROC IF id = ID_MAIN THEN IDS_MAIN ELSE SUPER self.id2input(id)

PROC getinput(id) OF in1out0
ENDPROC IF id = ID_MAIN THEN self.in ELSE SUPER self.getinput(id)

PROC clear() OF in1out0 IS clearlink(self.in) BUT SUPER self.clear()

PROC isready() OF in1out0
ENDPROC IF self.in.ready THEN SUPER self.isready() ELSE FALSE

/*-------------------------------------------------------------------------*/

PROC check() OF in1out0
	IF SUPER self.check() = FALSE THEN RETURN FALSE
	IF self.in.from = NIL
		Throw(ERR_CHECK, [ CHECK_INPUT_NOT_CONNECTED, self, ID_MAIN ])
	ENDIF
ENDPROC TRUE

/*--------------------------------------------------------------------------+
| END: in1out0.e                                                            |
+==========================================================================*/
