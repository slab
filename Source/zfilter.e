/*==========================================================================+
| zfilter.e                                                                 |
| Effect class "zfilter"                                                    |
| z-plane filters, see mathematical appendix                                |
|                                                                           |
| Parameters:                                                               |
| "poles"       0 <= number of poles  <= 31     \ limit due to number of    |
| "zeros"       0 <= number of zeros  <= 31     / unsigned bits in a LONG   |
| "poleXr"               0 <= pole X radius    <  1                         |
| "poleXf"      - rate / 2 <  pole X frequency <= rate / 2                  |
| "zeroXr"               0 <= zero X radius    <  1                         |
| "zeroXf"      - rate / 2 <  zero X frequency <= rate / 2                  |
+--------------------------------------------------------------------------*/

OPT MODULE, PREPROCESS

MODULE '*cbuffer', '*defs', '*in1out1', '*link', '*string', '*value',
       '*debug'

/*-------------------------------------------------------------------------*/

-> Static arrays make all objects large, but make implementation much simpler
EXPORT OBJECT zfilter OF in1out1
PRIVATE
	p      : LONG               -> poles (integer)
	z      : LONG               -> zeros (integer)
	pf[32] : ARRAY OF LONG      -> pole freq     \ polar form, as set
	pr[32] : ARRAY OF LONG      -> pole radius   |
	zf[32] : ARRAY OF LONG      -> zero freq     |
	zr[32] : ARRAY OF LONG      -> zero radius   /
	px[32] : ARRAY OF LONG      -> pole x        \ cartesian form, for calc
	py[32] : ARRAY OF LONG      -> pole y        |
	zx[32] : ARRAY OF LONG      -> zero x        |
	zy[32] : ARRAY OF LONG      -> zero y        /
	rx[32] : ARRAY OF LONG      -> recursion x
	ry[32] : ARRAY OF LONG      -> recursion y (ry[0] not used)
	x      : PTR TO cbuffer     -> previous input samples
	y      : PTR TO cbuffer     -> previous output samples
ENDOBJECT

PROC class() OF zfilter IS 'zfilter'

/*-------------------------------------------------------------------------*/

PROC new(list, name) OF zfilter
	SUPER self.new(list, name)
	self.p := 0     -> empty filter, out = in
	self.z := 0
	-> arrays set before use, no need to clear
	NEW self.x.new(32.0)    -> maximum length
	NEW self.y.new(32.0)
ENDPROC

/*-------------------------------------------------------------------------*/

PROC end() OF zfilter
	END self.y              -> delete sample buffers
	END self.x
ENDPROC SUPER self.end()

/*-------------------------------------------------------------------------*/

PROC reset() OF zfilter
	SUPER self.reset()
	assert(self.x, 'zfilter.reset.x')
	assert(self.y, 'zfilter.reset.y')
	self.x.clear()          -> clear sample buffers
	self.y.clear()
ENDPROC

/*-------------------------------------------------------------------------*/

PROC param2id(str : PTR TO CHAR) OF zfilter
	DEF id, len
	IF     strcmp(IDS_POLES, str); RETURN ID_POLES  -> these must be before..
	ELSEIF strcmp(IDS_ZEROS, str); RETURN ID_ZEROS
	ELSEIF strncmp(IDS_POLE, str, 4)                -> .. these
		-> poleXr, poleXf
		id, len := Val(str + 4)
		IF (len > 0) AND (0 < id) AND (id <= self.p)
			IF     strcmp('r', str + 4 + len); RETURN ID_MULTI_POLE_R + id
			ELSEIF strcmp('f', str + 4 + len); RETURN ID_MULTI_POLE_F + id
			ENDIF
		ENDIF
	ELSEIF strncmp(IDS_ZERO, str, 4)
		-> zeroXr, zeroXf
		id, len := Val(str + 4)
		IF (len > 0) AND (0 < id) AND (id <= self.z)
			IF     strcmp('r', str + 4 + len); RETURN ID_MULTI_ZERO_R + id
			ELSEIF strcmp('f', str + 4 + len); RETURN ID_MULTI_ZERO_F + id
			ENDIF
		ENDIF
	ENDIF
ENDPROC SUPER self.param2id(str)

/*-------------------------------------------------------------------------*/

PROC id2param(id) OF zfilter
	IF     (ID_MULTI_ZERO_R < id) AND (id <= (ID_MULTI_ZERO_R + self.z))
		RETURN StringF(String(8), 'zero\dr', id - ID_MULTI_ZERO_R)
	ELSEIF (ID_MULTI_ZERO_F < id) AND (id <= (ID_MULTI_ZERO_F + self.z))
		RETURN StringF(String(8), 'zero\df', id - ID_MULTI_ZERO_F)
	ELSEIF (ID_MULTI_POLE_R < id) AND (id <= (ID_MULTI_POLE_R + self.p))
		RETURN StringF(String(8), 'pole\dr', id - ID_MULTI_POLE_R)
	ELSEIF (ID_MULTI_POLE_F < id) AND (id <= (ID_MULTI_POLE_F + self.p))
		RETURN StringF(String(8), 'pole\df', id - ID_MULTI_POLE_F)
	ENDIF
ENDPROC SUPER self.id2param(id)

/*-------------------------------------------------------------------------*/

PROC paramtype(id) OF zfilter
	IF ((ID_MULTI_ZERO_R < id) AND (id <= (ID_MULTI_ZERO_R + self.z))) OR
	   ((ID_MULTI_ZERO_F < id) AND (id <= (ID_MULTI_ZERO_F + self.z))) OR
	   ((ID_MULTI_POLE_R < id) AND (id <= (ID_MULTI_POLE_R + self.p))) OR
	   ((ID_MULTI_POLE_F < id) AND (id <= (ID_MULTI_POLE_F + self.p))) OR
	   (id = ID_POLES) OR (id = ID_ZEROS)
		RETURN TYPE_NUMBER
	ENDIF
ENDPROC SUPER self.paramtype(id)

/*-------------------------------------------------------------------------*/

PROC set(id, data) OF zfilter
	IF     (ID_MULTI_ZERO_R < id) AND (id <= (ID_MULTI_ZERO_R + self.z))
		self.zr[id - 1 - ID_MULTI_ZERO_R] := data
		self.setrecalc()
	ELSEIF (ID_MULTI_ZERO_F < id) AND (id <= (ID_MULTI_ZERO_F + self.z))
		self.zf[id - 1 - ID_MULTI_ZERO_F] := data
		self.setrecalc()
	ELSEIF (ID_MULTI_POLE_R < id) AND (id <= (ID_MULTI_POLE_R + self.p))
		self.pr[id - 1 - ID_MULTI_POLE_R] := data
		self.setrecalc()
	ELSEIF (ID_MULTI_POLE_F < id) AND (id <= (ID_MULTI_POLE_F + self.p))
		self.pf[id - 1 - ID_MULTI_POLE_F] := data
		self.setrecalc()
	ELSEIF id = ID_POLES
		self.p := ! data !
		self.setrecalc()
	ELSEIF id = ID_ZEROS
		self.z := ! data !
		self.setrecalc()
	ELSE
		SUPER self.set(id, data)
	ENDIF
ENDPROC

/*-------------------------------------------------------------------------*/

PROC get(id) OF zfilter
	IF     (ID_MULTI_ZERO_R < id) AND (id <= (ID_MULTI_ZERO_R + self.z))
		RETURN self.zr[id - 1 - ID_MULTI_ZERO_R]
	ELSEIF (ID_MULTI_ZERO_F < id) AND (id <= (ID_MULTI_ZERO_F + self.z))
		RETURN self.zf[id - 1 - ID_MULTI_ZERO_F]
	ELSEIF (ID_MULTI_POLE_R < id) AND (id <= (ID_MULTI_POLE_R + self.p))
		RETURN self.pr[id - 1 - ID_MULTI_POLE_R]
	ELSEIF (ID_MULTI_POLE_F < id) AND (id <= (ID_MULTI_POLE_F + self.p))
		RETURN self.pf[id - 1 - ID_MULTI_POLE_F]
	ELSEIF id = ID_POLES
		RETURN self.p !
	ELSEIF id = ID_ZEROS
		RETURN self.z !
	ENDIF
ENDPROC SUPER self.get(id)

/*-------------------------------------------------------------------------*/

PROC process() OF zfilter
	DEF out, i
	SUPER self.process()
	-> move to next in buffer
	self.x.next()
	self.y.next()
	self.x.write(self._main())
	-> calculate
	out := 0.0
	IF self.z
		FOR i := 0 TO self.z
			out := ! out + (! self.rx[i] * self.x.readrel(i - self.z !))
		ENDFOR
	ENDIF
	IF self.p
		FOR i := 1 TO self.p
			out := ! out - (! self.ry[i] * self.y.readrel(i - self.p !))
		ENDFOR
		out := ! out / self.ry[0]
	ENDIF
	-> output
	self.y.write(out)
	self.output(ID_MAIN, out)
ENDPROC

/*-------------------------------------------------------------------------*/

PROC recalc() OF zfilter
	DEF i, link : PTR TO link, t
	SUPER self.recalc()
	link := self.getinput(ID_MAIN)
	t := ! PI2 / link.rate
	FOR i := 0 TO self.p - 1        -> sort out poles
		self.px[i] := ! self.pr[i] * Fcos(! self.pf[i] * t)
		self.py[i] := ! self.pr[i] * Fsin(! self.pf[i] * t)
	ENDFOR
	FOR i := 0 TO self.z - 1        -> sort out zeros
		self.zx[i] := ! self.zr[i] * Fcos(! self.zf[i] * t)
		self.zy[i] := ! self.zr[i] * Fsin(! self.zf[i] * t)
	ENDFOR
	-> make recursion formulae
	IF self.p THEN make_r(self.p, self.px, self.py, self.ry)
	IF self.z THEN make_r(self.z, self.zx, self.zy, self.rx)
	-> set delays
	self.x.setlength(self.z !)
	self.y.setlength(self.p !)

	-> debugging code (dumps all relevant state info)
#ifdef DEBUG
	PrintF(DEBUG'zfilter.recalc zeros = \d\n', self.z)
	PrintF(DEBUG'\ti\tzr\tzf\tzx\tzy\n')
	FOR i := 0 TO self.z - 1 DO PrintF(DEBUG'\t\d\t\s\t\s\t\s\t\s\n', i,
	            realf(self.zr[i]), realf(self.zf[i]),
	            realf(self.zx[i]), realf(self.zy[i]))
	PrintF(DEBUG'zfilter.recalc poles = \d\n', self.p)
	PrintF(DEBUG'\ti\tpr\tpf\tpx\tpy\n')
	FOR i := 0 TO self.p - 1 DO PrintF(DEBUG'\t\d\t\s\t\s\t\s\t\s\n', i,
	            realf(self.pr[i]), realf(self.pf[i]),
	            realf(self.px[i]), realf(self.py[i]))
	PrintF(DEBUG'zfilter.recalc recurse\n')
	PrintF(DEBUG'\ti\trx\try\n')
	FOR i := 0 TO IF self.z > self.p THEN self.z ELSE self.p
		PrintF(DEBUG'\t\d\t\s\t\s\n', i,
		        IF i <= self.z THEN realf(self.rx[i]) ELSE '-',
		        IF i <= self.p THEN realf(self.ry[i]) ELSE '-')
	ENDFOR
#endif
ENDPROC

/*-------------------------------------------------------------------------*/

-> Multiply out a set of linear factors, assuming result is real polynomial
-> Polynomial is d[0]z^n + d[1]z^(n-1) + ... + d[n]
-> This function is a candidate for assembly language optimisation, as the
-> number of loops is high (O(factors * 2 ^ factors)) and only simple
-> maths is done (no large function calls)
-> fx, fy are length factors, d is length factors + 1
PROC make_r(factors, fx : PTR TO LONG, fy : PTR TO LONG, d : PTR TO LONG)
	DEF i : REG, j : REG, k : REG, l : REG, m : REG, -> put ints in registers
	    x, y, xx, yy, a, b
	assert(factors, 'zfilter.make_r.factors')
	assert(fx,      'zfilter.make_r.fx')
	assert(fy,      'zfilter.make_r.fy')
	assert(d,       'zfilter.make_r.d')
    FOR i := 0 TO factors DO d[i] := 0.0    -> clear
	FOR i := 0 TO Shl(1, factors) - 1       -> all permutations (large)
		k := i                              -> copy of i to modify
		m := 0                              -> number of numbers multiplied
		x := 1.0                            -> multiply => start at 1
		y := 0.0
		FOR j := 0 TO factors - 1
			-> extract bits in order
			l, k := Mod(k, 2)  -> l = multiply by a number (1) or z (0)
			-> k := k / 2      -> truncated integer division in Mod
			m := m + l
			IF l = 1
				a := ! -fx[j]       -> multiply (x,y) by (fx,fy)[j]
				b := ! -fy[j]
				xx := ! (! a * x) - (! b * y)
				yy := ! (! a * y) + (! b * x)
				x := xx
				y := yy
			-> ELSE                 -> multiply by "z"
			ENDIF
		ENDFOR
		d[m] := ! d[m] + x      -> add to polynomial coefficient
	ENDFOR
ENDPROC

/*--------------------------------------------------------------------------+
| END: zfilter.e                                                            |
+==========================================================================*/
