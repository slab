/*==========================================================================+
| constant.e                                                                |
| Effect class "constant", outputs a constant, set by parameter "value"     |
+--------------------------------------------------------------------------*/

OPT MODULE, PREPROCESS

MODULE '*defs', '*in0out1', '*string', '*value'

/*-------------------------------------------------------------------------*/

EXPORT OBJECT constant OF in0out1
PRIVATE
	value   : LONG
ENDOBJECT

PROC class() OF constant IS 'constant'

/*-------------------------------------------------------------------------*/

PROC new(list, name) OF constant
	SUPER self.new(list, name)
	self.value := 0.0
ENDPROC

/*-------------------------------------------------------------------------*/

PROC param2id(str) OF constant
ENDPROC IF strcmp(IDS_VALUE, str) THEN ID_VALUE ELSE SUPER self.param2id(str)

PROC id2param(id) OF constant
ENDPROC IF id = ID_VALUE THEN IDS_VALUE ELSE SUPER self.id2param(id)

PROC paramtype(id) OF constant
ENDPROC IF id = ID_VALUE THEN TYPE_NUMBER ELSE SUPER self.paramtype(id)

PROC get(id) OF constant
ENDPROC IF id = ID_VALUE THEN self.value ELSE SUPER self.get(id)

/*-------------------------------------------------------------------------*/

PROC set(id, data) OF constant
	SELECT id
	CASE ID_VALUE;  self.value := data
	DEFAULT;        SUPER self.set(id, data)
	ENDSELECT
ENDPROC

/*-------------------------------------------------------------------------*/

PROC process() OF constant
	SUPER self.process()
	self.output(ID_MAIN, self.value)
ENDPROC

/*--------------------------------------------------------------------------+
| END: constant.e                                                           |
+==========================================================================*/
