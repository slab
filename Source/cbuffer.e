/*==========================================================================+
| cbuffer.e                                                                 |
| Circular buffer class                                                     |
|                                                                           |
| For correct behaviour with short lengths, write read next should be done  |
| in that order (or rotation: rnw, nwr)                                     |
|                                                                           |
| cbuffer.new(length)           constructor, length is float, 1.5 times the |
|                               length is allocated                         |
| cbuffer.end()                 destructor                                  |
| cbuffer.length()              get the current length (float)              |
| cbuffer.setlength(length)     set the length (float), read position is    |
|                               changed, memory is reallocated if necessary |
| cbuffer.read()                read from the current read position, using  |
|                               linear interpolation                        |
| cbuffer.readrel(offset)       read relative (offset is float) to the      |
|                               current read position, using linear         |
|                               interpolation ( -length <= offset <= 0.0)   |
| cbuffer.write(data)           store (float) at the current write position |
| cbuffer.next()                move to next position in buffer             |
| cbuffer.clear()               reset contents to zero                      |
|                                                                           |
+--------------------------------------------------------------------------*/

OPT MODULE, PREPROCESS

MODULE '*debug'

RAISE "MEM" IF New() = NIL      -> Automatic exceptions

/*-------------------------------------------------------------------------*/

EXPORT OBJECT cbuffer
PRIVATE
	maxlen  : LONG          -> integer physical length
	l       : LONG          -> float length
	r       : LONG          -> float read position
	w       : LONG          -> integer write position
	data    : PTR TO LONG   -> floats
ENDOBJECT

-> E cannot have float constants
#define MINLEN 16.0

/*-------------------------------------------------------------------------*/

PROC new(length) OF cbuffer      -> length is float
	assert(! length >= 0.0, 'cbuffer.new')
	self.maxlen := ! (! length * 1.5) + MINLEN !   -> allow space for growth
	self.l := length
	self.r := self.maxlen - 1 ! - length
	self.w := self.maxlen - 1
	self.data := New(self.maxlen * SIZEOF LONG)
ENDPROC

/*-------------------------------------------------------------------------*/

PROC end() OF cbuffer IS Dispose(self.data)

/*-------------------------------------------------------------------------*/

PROC length() OF cbuffer IS self.l

/*-------------------------------------------------------------------------*/

PROC setlength(length) OF cbuffer       -> length is float
	DEF newmaxlen, newdata : PTR TO LONG, i, offset
	assert(! length >= 0.0, 'cbuffer.setlength')
	IF ! length ! < (self.maxlen - 2)
		-> Enough space, change pointer
		self.r := wrapf(! self.r + self.l - length, self.maxlen !)
		self.l := length
	ELSE
		-> Not enough space, allocate new
		newmaxlen := ! (! length * 1.5) + MINLEN !
		newdata   := New(newmaxlen * SIZEOF LONG)
		-> Copy data up to (including) write position to end of buffer
		-> (Correct, consider new index for max i)
		offset := newmaxlen - self.w - 1
		FOR i := 0 TO self.w
			newdata[offset + i] := self.data[i]
		ENDFOR
		-> Copy data after write pointer (ie, long before) to before that
		-> (Correct, consider new index for max i, and above index at i = 0)
		offset := newmaxlen - self.w - 1 - self.maxlen
		FOR i := self.w + 1 TO self.maxlen - 1
			newdata[offset + i] := self.data[i]
		ENDFOR
		-> Clear rest of buffer
		FOR i := 0 TO newmaxlen - self.w - self.maxlen - 2
			newdata[i] := 0.0
		ENDFOR
		-> Replace old with new
		Dispose(self.data)
		self.maxlen := newmaxlen
		self.data   := newdata
		self.r      := newmaxlen - 1 ! - length
		self.w      := newmaxlen - 1
		self.l      := length
	ENDIF
ENDPROC

/*-------------------------------------------------------------------------*/

PROC read() OF cbuffer                  -> using linear interpolation
	DEF x0, x1, y0, y1, dx, dy, y
	assert(self.data, 'cbuffer.read.data')
	x0 := ! wrapf(Ffloor(self.r), self.maxlen !) !
	x1 := ! wrapf(Fceil (self.r), self.maxlen !) !
	y0 := self.data[x0]
	y1 := self.data[x1]
	dx := ! self.r - Ffloor(self.r)
	dy := ! y1 - y0
	y  := ! y0 + (! dx * dy)
ENDPROC y

/*-------------------------------------------------------------------------*/

PROC readrel(offset) OF cbuffer        -> using linear interpolation
	DEF r, x0, x1, y0, y1, dx, dy, y
	assert((!-self.l<=offset) AND (!offset<=0.0),'cbuffer.readrel.offset')
	assert(self.data, 'cbuffer.readrel.data')
	r  := ! self.r - offset
	x0 := ! wrapf(Ffloor(r), self.maxlen !) !
	x1 := ! wrapf(Fceil (r), self.maxlen !) !
	y0 := self.data[x0]
	y1 := self.data[x1]
	dx := ! r - Ffloor(r)
	dy := ! y1 - y0
	y  := ! y0 + (! dx * dy)
ENDPROC y

/*-------------------------------------------------------------------------*/

PROC write(data) OF cbuffer
	self.data[self.w] := data
ENDPROC

/*-------------------------------------------------------------------------*/

PROC next() OF cbuffer
	self.w := wrapi(self.w + 1, self.maxlen)
	self.r := wrapf(! self.r + 1.0, self.maxlen !)
	assert((0 <= self.w) AND (self.w < self.maxlen), 'cbuffer.next.w')
	assert((!0.0 <= self.r) AND (!self.r < (self.maxlen!)), 'cbuffer.next.r')
ENDPROC

/*-------------------------------------------------------------------------*/

PROC clear() OF cbuffer
	DEF i
	assert(self.data, 'cbuffer.clear')
	FOR i := 0 TO self.maxlen - 1 DO self.data[i] := 0.0
ENDPROC

/*-------------------------------------------------------------------------*/

-> Wrap a float to between 0.0 and length (length > 0.0)
PROC wrapf(x, length)
	assert(! length > 0.0, 'cbuffer.wrapf.length')
	WHILE ! x >= length
		x := ! x - length
	ENDWHILE
	WHILE ! x < 0.0
		x := ! x + length
	ENDWHILE
	assert((! 0.0 <= x) AND (! x < length), 'cbuffer.wrapf.x')
ENDPROC x

/*-------------------------------------------------------------------------*/

-> Wrap an integer to between 0 and length (length > 0)
PROC wrapi(x, length)
	assert(length > 0, 'cbuffer.wrapi.length')
	WHILE x >= length
		x := x - length
	ENDWHILE
	WHILE x < 0
		x := x + length
	ENDWHILE
	assert((0 <= x) AND (x < length), 'cbuffer.wrapi.x')
ENDPROC x

/*--------------------------------------------------------------------------+
| END: cbuffer.e                                                            |
+==========================================================================*/
