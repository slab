/*==========================================================================+
| in0out1.e                                                                 |
| Effect base class in0out1                                                 |
| Features: output "main", parameter "rate" = output sample rate            |
+--------------------------------------------------------------------------*/

OPT MODULE, PREPROCESS

MODULE '*effect', '*defs', '*link', '*string', '*value'

/*-------------------------------------------------------------------------*/

EXPORT OBJECT in0out1 OF effect
PUBLIC
	rate : LONG         -> Needed by some derived classes
PRIVATE
	out  : PTR TO link
ENDOBJECT

PROC class()    OF in0out1 IS 'in0out1'
PROC issource() OF in0out1 IS TRUE

/*-------------------------------------------------------------------------*/

PROC new(list, name) OF in0out1
	SUPER self.new(list, name)
	self.out := NIL
	self.rate := DEF_RATE
ENDPROC

/*-------------------------------------------------------------------------*/

PROC end() OF in0out1
	IF self.out
		unlink(self.out)
		self.out := NIL
	ENDIF
ENDPROC SUPER self.end()

/*-------------------------------------------------------------------------*/

PROC output2id(str) OF in0out1
ENDPROC IF strcmp(IDS_MAIN, str) THEN ID_MAIN ELSE SUPER self.output2id(str)

PROC id2output(id) OF in0out1
ENDPROC IF id = ID_MAIN THEN IDS_MAIN ELSE SUPER self.id2output(id)

PROC param2id(str) OF in0out1
ENDPROC IF strcmp(IDS_RATE, str) THEN ID_RATE ELSE SUPER self.param2id(str)

PROC id2param(id) OF in0out1
ENDPROC IF id = ID_RATE THEN IDS_RATE ELSE SUPER self.id2param(id)

PROC paramtype(id) OF in0out1
ENDPROC IF id = ID_RATE THEN TYPE_NUMBER ELSE SUPER self.paramtype(id)

PROC get(id) OF in0out1
ENDPROC IF id = ID_RATE THEN self.rate ELSE SUPER self.get(id)

/*-------------------------------------------------------------------------*/

PROC setoutput(id, link) OF in0out1
	SELECT id
	CASE ID_MAIN;   self.out := link
	DEFAULT;        SUPER self.setoutput(id, link)
	ENDSELECT
ENDPROC

/*-------------------------------------------------------------------------*/

PROC output(id, data)   OF in0out1
	SELECT id
	CASE ID_MAIN;   outputlink(self.out, data)
	DEFAULT;        SUPER self.output(id, data)
	ENDSELECT
ENDPROC

/*-------------------------------------------------------------------------*/

PROC set(id, data) OF in0out1
	SELECT id
	CASE ID_RATE
		IF ! data <= 0.0 THEN Throw(ERR_BAD_RANGE, id)
		self.rate := data
	DEFAULT;       SUPER self.set(id, data)
	ENDSELECT
ENDPROC

/*-------------------------------------------------------------------------*/

PROC check() OF in0out1
	IF SUPER self.check() = FALSE THEN RETURN FALSE
	IF self.out = NIL
		Throw(ERR_CHECK, [ CHECK_OUTPUT_NOT_CONNECTED, self, ID_MAIN ])
	ENDIF
	self.out.rate := self.rate
ENDPROC self.out.to.check()

/*--------------------------------------------------------------------------+
| END: in0out1.e                                                            |
+==========================================================================*/
