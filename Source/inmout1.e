/*==========================================================================+
| inmout1.e                                                                 |
| Effect base class inmout1                                                 |
|                                                                           |
| Multiple inputs, single output.  The number of inputs can be set once,    |
| after which attempts to change it fail.                                   |
|                                                                           |
| Defines an output "main" and inputs "inX", X = 1, 2, ... "inputs" param   |
|                                                                           |
| inmout1._inputs()     get number of inputs (for efficiency in process)    |
| inmout1._in(x)        get input sample (for efficiency in process)        |
|                                                                           |
+--------------------------------------------------------------------------*/

OPT MODULE, PREPROCESS

MODULE '*effect', '*defs', '*link', '*string', '*value', '*debug'

RAISE "MEM" IF New() = NIL

/*-------------------------------------------------------------------------*/

EXPORT OBJECT inmout1 OF effect
PRIVATE
	ins : LONG
	in : PTR TO link
	out : PTR TO link
ENDOBJECT

PROC class()   OF inmout1 IS 'inmout1'
PROC _inputs() OF inmout1 IS self.ins
PROC _in(x)    OF inmout1 IS self.in[x - 1].data

/*-------------------------------------------------------------------------*/

PROC new(list, name) OF inmout1
	SUPER self.new(list, name)
	self.ins := 0
	self.in  := NIL
	self.out := NIL
ENDPROC

/*-------------------------------------------------------------------------*/

PROC end() OF inmout1
	DEF i
	IF self.in
		FOR i := 0 TO self.ins - 1
			IF self.in[i] THEN unlink(self.in[i])
		ENDFOR
		Dispose(self.in)
		self.in := NIL
	ENDIF
	IF self.out
		unlink(self.out)
		self.out := NIL
	ENDIF
ENDPROC SUPER self.end()

/*-------------------------------------------------------------------------*/

PROC input2id (str) OF inmout1
	DEF id, len
	IF strncmp(IDS_IN, str, 2)
		id, len := Val(str + 2)
		IF ((len + 2) = StrLen(str)) AND (0 < id) AND (id <= self.ins)
			RETURN ID_MULTI_IN + id
		ENDIF
	ENDIF
ENDPROC SUPER self.input2id(str)

/*-------------------------------------------------------------------------*/

PROC param2id(str) OF inmout1
	IF strcmp(IDS_INPUTS, str) THEN RETURN ID_INPUTS
ENDPROC SUPER self.param2id(str)

/*-------------------------------------------------------------------------*/

PROC id2input (id) OF inmout1
	DEF inid
	inid := id - ID_MULTI_IN
	IF (0 < inid) AND (inid <= self.ins)
		RETURN StringF(String(8), 'in\d', inid)
	ENDIF
ENDPROC SUPER self.id2input(id)

/*-------------------------------------------------------------------------*/

PROC output2id(str)     OF inmout1
ENDPROC IF strcmp(IDS_MAIN, str) THEN ID_MAIN ELSE SUPER self.output2id(str)

PROC id2output(id)      OF inmout1
ENDPROC IF id = ID_MAIN THEN IDS_MAIN ELSE SUPER self.id2output(id)

PROC id2param(id) OF inmout1
ENDPROC IF id = ID_INPUTS THEN IDS_INPUTS ELSE SUPER self.id2param(id)

PROC paramtype(id) OF inmout1
ENDPROC IF id = ID_INPUTS THEN TYPE_NUMBER ELSE SUPER self.paramtype(id)

PROC get(id) OF inmout1
ENDPROC IF id = ID_INPUTS THEN self.ins ELSE SUPER self.get(id)

/*-------------------------------------------------------------------------*/

PROC set(id, data) OF inmout1
	DEF i
	SELECT id
	CASE ID_INPUTS
		IF ! data ! <= 0 THEN Throw(ERR_BAD_RANGE, id)
		-> Cannot change number of inputs after first set (requires either
		-> relinking of all existing links (which requires access to kernel)
		-> or keeping all existing links and adding (which is tricky))
		IF self.in THEN RETURN FALSE
		self.in := New(! data ! * SIZEOF link)
		self.ins := ! data !
		-> Clear links
		FOR i := 0 TO self.ins - 1
			newlink(self.in[i], self, ID_MULTI_IN + i + 1)
		ENDFOR
		RETURN TRUE
	ENDSELECT
ENDPROC SUPER self.set(id, data)

/*-------------------------------------------------------------------------*/

PROC getinput(id)       OF inmout1
	DEF inid
	inid := id - ID_MULTI_IN
	IF (0 < inid) AND (inid <= self.ins)
		RETURN self.in[inid - 1]
	ENDIF
ENDPROC SUPER self.getinput(id)

/*-------------------------------------------------------------------------*/

PROC setoutput(id, link)    OF inmout1
	SELECT id
	CASE ID_MAIN;   self.out := link
	DEFAULT;        SUPER self.setoutput(id, link)
	ENDSELECT
ENDPROC

/*-------------------------------------------------------------------------*/

PROC clear() OF inmout1
	DEF i
	FOR i := 0 TO self.ins - 1
		clearlink(self.in[i])
	ENDFOR
ENDPROC SUPER self.clear()

/*-------------------------------------------------------------------------*/

PROC isready()          OF inmout1
	DEF ready = TRUE, i
	FOR i := 0 TO self.ins - 1
	EXIT ready = FALSE
		ready := ready AND self.in[i].ready
	ENDFOR
	IF ready
		RETURN SUPER self.isready()
	ENDIF
ENDPROC FALSE

/*-------------------------------------------------------------------------*/

PROC output(id, data)   OF inmout1
	SELECT id
	CASE ID_MAIN;   outputlink(self.out, data)
	DEFAULT;        SUPER self.output(id, data)
	ENDSELECT
ENDPROC

/*-------------------------------------------------------------------------*/

PROC check() OF inmout1
	DEF i
	IF SUPER self.check() = FALSE THEN RETURN FALSE
	IF self.out = NIL
		Throw(ERR_CHECK, [ CHECK_OUTPUT_NOT_CONNECTED, self, ID_MAIN ])
	ENDIF
	self.out.rate := self.in[0].rate
	IF self.out.to.check() = FALSE THEN RETURN FALSE
	IF self.in = NIL
		Throw(ERR_CHECK, [ CHECK_INTERNAL_ERROR, self, 'no inputs' ])
	ENDIF
	FOR i := 0 TO self.ins - 1
		IF self.in[i].from = NIL
			Throw(ERR_CHECK,
			     [ CHECK_INPUT_NOT_CONNECTED, self, i + 1 + ID_MULTI_IN ])
		ENDIF
	ENDFOR
	FOR i := 0 TO self.ins - 1
		IF ! self.in[i].rate <> self.in[0].rate
debug(PrintF(DEBUG'inmout1.check bad rate: \s <> \s\n',realf(self.in[i].rate), realf(self.in[0].rate)))
/*			Throw(ERR_CHECK,
			     [ CHECK_BAD_INPUT_RATE, self, i + 1 + ID_MULTI_IN ])
*/		ENDIF
	ENDFOR
ENDPROC TRUE

/*--------------------------------------------------------------------------+
| END: inmout1.e                                                            |
+==========================================================================*/
