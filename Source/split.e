/*==========================================================================+
| split.e                                                                   |
| Effect class "split", sends input to all outputs                          |
+--------------------------------------------------------------------------*/

OPT MODULE

MODULE '*defs', '*in1outm', '*debug'

EXPORT OBJECT split OF in1outm
ENDOBJECT

PROC class() OF split IS 'split'

PROC process() OF split
	DEF i
	SUPER self.process()
	FOR i := 1 TO self._outputs() DO self.output(self._out(i), self._in())
ENDPROC

/*--------------------------------------------------------------------------+
| END: split.e                                                              |
+==========================================================================*/
