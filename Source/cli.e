/*==========================================================================+
| cli.e                                                                     |
| Command line interface                                                    |
|                                                                           |
| cli.new(in, out, err)     constructor; in, out, err are files for text IO |
| cli.end()                 destructor (called via END)                     |
| cli.parse(str)            parse and execute a command string, terminated  |
|                           in "\n"; the string is modified; returns TRUE   |
|                           if command was quit                             |
|                                                                           |
| The following should not be called from outside the class                 |
|                                                                           |
| cli.parsenew(str)         \   parse the arguments (str) for the command   |
| cli.parsedelete(str)      |   and execute it                              |
| cli.parselink(str)        |                                               |
| cli.parseset(str)         |                                               |
| cli.parserun(str)         |                                               |
| cli.parselist(str)        /                                               |
| cli.getident(str)         (destructively) get an identifier, returning    |
|                           the identifier and the new location             |
| cli.error(err, info = 0)  print an error message for an error id          |
| cli.readargs(template, args, str, n)                                      |
|                           interface to dos.library/ReadArgs(), assumes    |
|                           n simple strings to be copied (uses String() to |
|                           allocate so free with DisposeLink()             |
|                                                                           |
+--------------------------------------------------------------------------*/

OPT MODULE, PREPROCESS

RAISE "MEM" IF String() = NIL

MODULE '*defs', '*kernel', '*list', '*string', '*value', '*effect', '*hack',
       'dos/dos', 'dos/rdargs', 'tools/ctype', '*debug'

/*-------------------------------------------------------------------------*/

EXPORT OBJECT cli
PRIVATE
	kernel  : PTR TO kernel
ENDOBJECT

/*-------------------------------------------------------------------------*/

PROC new() OF cli
	NEW self.kernel.new()
ENDPROC

/*-------------------------------------------------------------------------*/

PROC end() OF cli
	IF self.kernel THEN END self.kernel
ENDPROC

/*-------------------------------------------------------------------------*/

PROC parse(s : PTR TO CHAR) OF cli HANDLE
	DEF com
	-> Strip white space
	WHILE isspace(s[]) DO s++
	-> Check for empty command
	IF (s[0] = 0) OR (s[0] = "\n") THEN RETURN FALSE
	-> Get command (in length order for efficiency)
	com, s := self.getident(s)
	IF     strcmp('new',    com);   self.parsenew(s)
	ELSEIF strcmp('run',    com);   self.parserun(s)
	ELSEIF strcmp('set',    com);   self.parseset(s)
	ELSEIF strcmp('link',   com);   self.parselink(s)
	ELSEIF strcmp('list',   com);   self.parselist(s)
	ELSEIF strcmp('quit',   com);   RETURN TRUE
	ELSEIF strcmp('reset',  com);   self.kernel.reset()
	ELSEIF strcmp('delete', com);   self.parsedelete(s)
	ELSE;  Throw(ERR_UNKNOWN_COMMAND, com)
	ENDIF
EXCEPT
	self.error(exception, exceptioninfo)
ENDPROC FALSE

/*-------------------------------------------------------------------------*/

PROC parsenew(s : PTR TO CHAR) OF cli
	DEF args[2] : ARRAY OF LONG
	args[0] := NIL
	args[1] := NIL
	self.readargs('TYPE,NAME', args, s, 2)
	self.kernel.new_(args[0], args[1])
ENDPROC

/*-------------------------------------------------------------------------*/

PROC parsedelete(s : PTR TO CHAR) OF cli
	DEF args[1] : ARRAY OF LONG
	args[0] := NIL
	self.readargs('NAME', args, s, 1)
	self.kernel.delete(args[0])
ENDPROC

/*-------------------------------------------------------------------------*/

PROC parselink(s : PTR TO CHAR) OF cli
	DEF args[4] : ARRAY OF LONG
	args[0] := NIL
	args[1] := NIL
	args[2] := NIL
	args[3] := NIL
	self.readargs('FROM,OUTPUT,TO,INPUT', args, s, 4)
    self.kernel.link(args[0], args[1], args[2], args[3])
ENDPROC

/*-------------------------------------------------------------------------*/

PROC parseset(s : PTR TO CHAR) OF cli
	DEF args[3] : ARRAY OF LONG, type, num, len, val : value,
	    op : value_objpart, opargs[2] : ARRAY OF LONG, str : PTR TO CHAR
	args[0] := NIL
	args[1] := NIL
	args[2] := NIL
	self.readargs('NAME,PARAM,VAL/F', args, s, 3)     -> /F = rest of line
	val.type := self.kernel.paramtype(args[0], args[1])
	type := val.type
	SELECT type
	CASE TYPE_NUMBER
		num, len := RealVal(args[2])
		IF len <> StrLen(args[2]) THEN Throw(ERR_BAD_NUMBER, args[2])
		val.data := num
	CASE TYPE_STRING
		val.data := args[2]
	CASE TYPE_OBJPART
		-> put \n at end of string
		str := StrCopy(String(StrLen(args[2]) + 2), args[2])
		str[StrLen(args[2]) + 1] := 0
		str[StrLen(args[2])    ] := "\n"
		opargs[0] := NIL
		opargs[1] := NIL
		self.readargs('NAME,PART', opargs, str, 2)
		op.obj := opargs[0]
		op.pid := opargs[1]
		val.data := op
	DEFAULT
		Throw(ERR_BAD_PARAM_TYPE, type)
	ENDSELECT
	self.kernel.set_(args[0], args[1], val)
ENDPROC

/*-------------------------------------------------------------------------*/

PROC parserun(s : PTR TO CHAR) OF cli
	WHILE isspace(s[0]) AND (s[0] <> "\n") DO s++
	IF (s[0] <> "\n") AND (s[0] <> 0) THEN Throw(ERR_BAD_ARGS, '')
    self.kernel.run()
ENDPROC

/*-------------------------------------------------------------------------*/

PROC parselist(str) OF cli      -> compiler warning, unreferenced str
	DEF list : PTR TO list, node : PTR TO node, args[1] : ARRAY OF LONG,
	    fx : PTR TO effect
	args[0] := NIL
	self.readargs('NAME', args, str, 1)
	IF strcmp('.', args[0])
		PrintF('** Effect types:\n')
		list := self.kernel.classes
		node := list.head.next
		WHILE node.next
			PrintF('**\t\s\n', node.name)
			node := node.next
		ENDWHILE
	ELSEIF strcmp('*', args[0])
		PrintF('** Effect objects:\n')
		list := self.kernel.objects
		node := list.head.next
		WHILE node.next
			PrintF('**\t\s\n', node.name)
			node := node.next
		ENDWHILE
	ELSE
		-> List object parameters / type properties ?
		node := find(self.kernel.objects, args[0])
		IF node
			fx := __node2effect(node)
			PrintF('** Name: "\s"\n** Type: "\s"\n', node.name, fx.class())
		ENDIF
	ENDIF
ENDPROC

/*-------------------------------------------------------------------------*/

PROC getident(s : PTR TO CHAR) OF cli
	DEF ident
	ident := s
	IF isalpha(s[0])
		s++
		WHILE isalnum(s[0]) OR (s[0] = "_") DO s++
		IF isspace(s[0]) OR (s[0] = 0) OR (s[0] = "\n")
			s[0] := 0
			s++
			RETURN ident, s
		ENDIF
		-> s is at bad char
	ENDIF
	Throw(ERR_BAD_COMMAND_LINE, s)
ENDPROC

/*-------------------------------------------------------------------------*/

-> NB: dos.library/ReadArgs() expects the string to be \n terminated
-> The data *must* be copied from the structure returned before freeing
PROC readargs(template, args : PTR TO LONG, string, n) OF cli
	DEF rdargs : PTR TO rdargs, i
	IF rdargs := AllocDosObject(DOS_RDARGS, NIL)
	    rdargs.source.buffer := string
    	rdargs.source.length := StrLen(string)
    	IF ReadArgs(template, args, rdargs)
			FOR i := 0 TO n - 1     -> copy string
				args[i] := StrCopy(String(StrLen(args[i]) + 1), args[i])
			ENDFOR
			FreeArgs(rdargs)
    		FreeDosObject(DOS_RDARGS, rdargs)
	   	ELSE
    		FreeDosObject(DOS_RDARGS, rdargs)
	    	self.error(ERR_BAD_ARGS, template)
    	ENDIF
    ELSE
		Raise(ERR_ALLOC_RDARGS)
    ENDIF
ENDPROC

/*-------------------------------------------------------------------------*/

PROC error(error, data = NIL : PTR TO CHAR) OF cli
	DEF prompt, datal : PTR TO LONG, fx : PTR TO effect, id
	prompt := '** Error: '
	SELECT error
	CASE ERR_OK
		-> No error
	CASE ERR_NO_SUCH_CLASS
		PrintF('\seffect type "\s" not found\n', prompt, data)
	CASE ERR_NO_SUCH_NAME
		PrintF('\seffect object "\s" not found\n', prompt, data)
	CASE ERR_NAME_ALREADY_USED
		PrintF('\seffect object "\s" already exists\n', prompt, data)
	CASE ERR_NO_SUCH_INPUT
		PrintF('\sinput "\s" not found\n', prompt, data)
	CASE ERR_NO_SUCH_OUTPUT
		PrintF('\soutput "\s" not found\n', prompt, data)
	CASE ERR_NO_SUCH_PARAM
		PrintF('\sparameter "\s" not found\n', prompt, data)
	CASE ERR_UNKNOWN_COMMAND
		PrintF('\sunknown command "\s"\n', prompt, data)
	CASE ERR_BAD_ARGS
		PrintF('\sbad arguments for "\s"\n', prompt, data)
	CASE ERR_BAD_RANGE
		PrintF('\sparameter out of allowed range\n', prompt)
	CASE ERR_PARAM_NOT_NUMBER
		PrintF('\sparameter "\s" is not numeric\n', prompt, data)
	CASE ERR_BAD_COMMAND_LINE
		-> Print one character as unexpected
		PrintF('\sunexpected character "\s"\n', prompt, [data[0],0] : CHAR)
	CASE ERR_BAD_NUMBER
		PrintF('\snumber expected "\s"\n', prompt, data)
	CASE ERR_CHECK
		PrintF('\sprocessing failed, check linkage\n', prompt)
cli_error_check_container_recurse:      -> fake recursion for container
		datal := data
		error := datal[0]
		fx := datal[1]
		id := datal[2]
		SELECT error
		CASE CHECK_OK
			PrintF('\sno error?\n', prompt)
		CASE CHECK_OUTPUT_NOT_CONNECTED
			PrintF('\soutput "\s" of "\s" not connected\n', prompt,
	                    		            fx.id2output(id), fx.node.name)
		CASE CHECK_INPUT_NOT_CONNECTED
			PrintF('\sinput "\s" of "\s" not connected\n', prompt,
	                    		            fx.id2input(id), fx.node.name)
		CASE CHECK_INVALID_PARAM
			PrintF('\sparameter "\s" of "\s" is invalid\n', prompt,
	                    		            fx.id2param(id), fx.node.name)
		CASE CHECK_BAD_INPUT_RATE
			PrintF('\sinput "\s" of "\s" has a bad rate\n', prompt,
	                    		            fx.id2input(id), fx.node.name)
		CASE CHECK_INTERNAL_ERROR
			PrintF('\sinternal error\n', prompt)
		CASE CHECK_CONTAINER
			PrintF('\serror in "\s":\n', prompt, fx.node.name)
			data := id                  -> fake recursion
			JUMP cli_error_check_container_recurse
		DEFAULT
			PrintF('\sunknown\n')
		ENDSELECT
	DEFAULT
		Throw(error, data)
	ENDSELECT
ENDPROC

/*--------------------------------------------------------------------------+
| END: cli.e                                                                |
+==========================================================================*/
