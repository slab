/*==========================================================================+
| kernel.e                                                                  |
| System kernel                                                             |
|                                                                           |
| All arguments are strings unless otherwise specified.  Errors are         |
| reported by raising exceptions (see defs.e).                              |
|                                                                           |
| kernel.new()                  constructor                                 |
| kernel.end()                  destructor                                  |
| kernel.new_(class, name)      create a new effect object                  |
| kernel.delete(name)           delete a new effect object                  |
| kernel.link(source, output, dest, input)                                  |
|                               link effect objects                         |
| kernel.set_(name, param, value)                                           |
|                               set a parameter, value is a value structure |
| kernel.paramtype(name, param)                                             |
|                               get the type of a parameter                 |
| kernel.run()                  perform processing                          |
|                                                                           |
| kernel.runonce()              perform processing on one sample            |
| #?new(list, name)             classnode functions                         |
|                                                                           |
+--------------------------------------------------------------------------*/

OPT MODULE, PREPROCESS

/*-------------------------------------------------------------------------*/

MODULE '*classnode', '*defs', '*effect', '*hack', '*link', '*link_', '*list',
       '*string', '*value', '*debug',
       '*add', '*amp', '*bandpass', '*bandreject', '*constant', '*copy',
       '*delay', '*echo', '*fbdelay', '*feedback', '*halfrectify',
       '*highpass', '*invert', '*lowpass', '*mul', '*notch', '*print',
       '*pulse', '*ramp', '*read8svx', '*readslab', '*rectify', '*sine',
       '*split', '*toparam', '*triangle', '*vox', '*whitenoise',
       '*write8svx', '*writeslab', '*zfilter',
       '*exp', '*scale'

/*-------------------------------------------------------------------------*/

EXPORT OBJECT kernel
PUBLIC      -> CLI likes to list objects and classes
	classes : list
	objects : list
PRIVATE
	rate    : LONG      -> float sample rate to use for global calcs
	runtime : LONG      -> float time to run for
	runsmps : LONG      -> integer number of samples to run for
ENDOBJECT

/*-------------------------------------------------------------------------*/

-> Reduce verbosity of kernel.new
#define __addclass(name,func)\
add(self.classes, NEW [ NIL, NIL, 'name', {func} ] : classnode)

#define _addclass(name)\
add(self.classes, NEW [ NIL, NIL, 'name', {name} ] : classnode)

PROC new() OF kernel
	-> Initialise classes
	newlist(self.classes)
	__addclass(add, add_);
	_addclass(exp);         _addclass(scale);    ->_addclass(bandreject);
	_addclass(amp);         _addclass(bandpass);    _addclass(bandreject);
	_addclass(constant);    _addclass(copy);        _addclass(delay);
	_addclass(echo);        _addclass(fbdelay);     _addclass(feedback);
	_addclass(halfrectify); _addclass(highpass);    _addclass(invert);
	_addclass(lowpass);     _addclass(mul);         _addclass(notch);
	_addclass(print);       _addclass(pulse);       _addclass(ramp);
	_addclass(read8svx);    _addclass(readslab);    _addclass(rectify);
	_addclass(sine);        _addclass(split);       _addclass(toparam);
	_addclass(triangle);    _addclass(vox);         _addclass(whitenoise);
	_addclass(write8svx);   _addclass(writeslab);   _addclass(zfilter);
	-> Initialise object list
	newlist(self.objects)
	-> Initialise parameters
	self.rate := DEF_RATE
	self.runtime := 0.5
ENDPROC

/*-------------------------------------------------------------------------*/

PROC end() OF kernel
	DEF s : PTR TO effect, n : PTR TO node, c : PTR TO classnode, t
	-> Free objects
	n := self.objects.head.next
	WHILE (t := n.next) <> NIL
		s := __node2effect(n)
		END s
		n := t
	ENDWHILE
	-> Free classes
	c := self.classes.head.next
	WHILE (t := c.next) <> NIL
		END c
		c := t
	ENDWHILE
ENDPROC

/*-------------------------------------------------------------------------*/

-> Note "_", new() is used for constructors
PROC new_(class, name) OF kernel
	DEF c : PTR TO classnode, f
	IF (c := find(self.classes, class)) = NIL
		Throw(ERR_NO_SUCH_CLASS, class)
	ENDIF
	IF find(self.objects, name) THEN Throw(ERR_NAME_ALREADY_USED, name)
	f := c.newf
	-> compiler warning, variable used as function
	f(self.objects, name, NIL)      -> last variable is dummy for "_func"
ENDPROC

/*-------------------------------------------------------------------------*/

PROC delete(name) OF kernel
	DEF s   : PTR TO effect
	IF (s := __node2effect(find(self.objects, name))) = __node2effect(NIL)
		Throw(ERR_NO_SUCH_NAME, name)
	ENDIF
	END s
ENDPROC

/*-------------------------------------------------------------------------*/

PROC link(source, output, dest, input) OF kernel
	DEF s : PTR TO effect, sid, d : PTR TO effect, did
	IF (s := __node2effect(find(self.objects, source))) = __node2effect(NIL)
		Throw(ERR_NO_SUCH_NAME,   source)
	ENDIF
	IF (sid := s.output2id(output)) = ID_INVALID
		Throw(ERR_NO_SUCH_OUTPUT, output)
	ENDIF
	IF (d := __node2effect(find(self.objects, dest))) = __node2effect(NIL)
		Throw(ERR_NO_SUCH_NAME, dest)
	ENDIF
	IF (did := d.input2id(input)) = ID_INVALID
		Throw(ERR_NO_SUCH_INPUT, input)
	ENDIF
	link_(s, sid, d, did)       -> workaround for bug in EC
ENDPROC

/*-------------------------------------------------------------------------*/

-> Note "_", set() is to set parameters (for consistency with effect classes)
PROC set_(name, param, value : PTR TO value) OF kernel
	DEF s : PTR TO effect, id, obj
	IF (s := __node2effect(find(self.objects, name))) = __node2effect(NIL)
		Throw(ERR_NO_SUCH_NAME,   name)
	ENDIF
	IF (id := s.param2id(param)) = ID_INVALID
		Throw(ERR_NO_SUCH_PARAM, param)
	ENDIF
	IF value.type <> s.paramtype(id) THEN Throw(ERR_BAD_PARAM_TYPE, value)
	IF value.type = TYPE_OBJPART    -> get object pointer from string
		obj := find(self.objects, value.data::value_objpart.obj)
		IF obj = NIL
			Throw(ERR_NO_SUCH_NAME, value.data::value_objpart.obj)
		ENDIF
		value.data::value_objpart.obj := __node2effect(obj)
	ENDIF
	s.set(id, value.data)
ENDPROC

/*-------------------------------------------------------------------------*/

PROC paramtype(name, param) OF kernel
	DEF s : PTR TO effect, id
	IF (s := __node2effect(find(self.objects, name))) = __node2effect(NIL)
		Throw(ERR_NO_SUCH_NAME, name)
	ENDIF
	IF (id := s.param2id(param)) = ID_INVALID
		Throw(ERR_NO_SUCH_PARAM, param)
	ENDIF
ENDPROC s.paramtype(id)

/*-------------------------------------------------------------------------*/

PROC reset() OF kernel
	DEF n : PTR TO node, s : PTR TO effect
	n := self.objects.head.next
	WHILE n.next <> NIL
		s := __node2effect(n)
		s.reset()
		n := n.next
	ENDWHILE
ENDPROC

/*-------------------------------------------------------------------------*/

PROC run() OF kernel
	DEF n : PTR TO node, s : PTR TO effect, t
	-> Move any toparam objects to end of list
	n := self.objects.tail.prev
	WHILE (t := n.prev) <> NIL
		s := __node2effect(n)
		IF strcmp('toparam', s.class())
			remove(n)
			add(self.objects, n)
		ENDIF
		n := t
	ENDWHILE
	-> Check sources
	n := self.objects.head.next
	WHILE n.next <> NIL
		s := __node2effect(n)
		IF s.issource() THEN s.check()
		n := n.next
	ENDWHILE
/***************************************************************************/
	-> How many samples to run for
->	self.rate := 44100 ->maxsamplerate(self.objects.head.next)
->	self.runsmps := ! self.runtime * self.rate !
	self.runsmps := 22050
/***************************************************************************/
	-> Run
	FOR t := 1 TO self.runsmps DO self.runonce()
ENDPROC

/*-------------------------------------------------------------------------*/

PROC runonce() OF kernel
	DEF n : PTR TO node, s : PTR TO effect
	-> call process of sources
	n := self.objects.head.next
	WHILE n.next <> NIL
		s := __node2effect(n)
		IF s.issource() THEN s.process()
		n := n.next
	ENDWHILE
	-> clear all
	n := self.objects.head.next
	WHILE n.next <> NIL
		s := __node2effect(n)
		s.clear()
		n := n.next
	ENDWHILE
ENDPROC

/*-------------------------------------------------------------------------*/
/***************************************************************************/

PROC maxsamplerate(node : PTR TO node)
	DEF s : PTR TO effect, rate = 0.0, newrate
	WHILE node.next <> NIL
		s := __node2effect(node)
		IF s.issource()
			newrate := s.get(ID_RATE)
			IF ! newrate > rate THEN rate := newrate
		ENDIF
		node := node.next
	ENDWHILE
ENDPROC

/***************************************************************************/
/*-------------------------------------------------------------------------*/

-> classnode functions, generic macro (macros in E are limited to one line
-> without ";", so a dummy argument is used instead of DEF)
#define _func(type)\
PROC type(list, name, o = NIL : PTR TO type) IS NEW o.new(list, name)

-> already a function add() in list.e, so use add_()
PROC add_(list, name, o = NIL : PTR TO add) IS NEW o.new(list, name)

_func(exp);         _func(scale);    ->_func(bandreject);  _func(constant);
_func(amp);         _func(bandpass);    _func(bandreject);  _func(constant);
_func(copy);        _func(delay);       _func(echo);        _func(fbdelay);
_func(feedback);    _func(halfrectify); _func(highpass);    _func(invert);
_func(lowpass);     _func(mul);         _func(notch);       _func(print);
_func(pulse);       _func(ramp);        _func(read8svx);    _func(readslab);
_func(rectify);     _func(sine);        _func(split);       _func(toparam);
_func(triangle);    _func(vox);         _func(whitenoise);  _func(write8svx);
_func(writeslab);   _func(zfilter);

/*--------------------------------------------------------------------------+
| END: kernel.e                                                             |
+==========================================================================*/
