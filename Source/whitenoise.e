/*==========================================================================+
| whitenoise.e                                                              |
| Effect class "whitenoise", white noise (random values, between -1 and 1)  |
+--------------------------------------------------------------------------*/

OPT MODULE

MODULE '*defs', '*in0out1', '*rnd'

EXPORT OBJECT whitenoise OF in0out1
ENDOBJECT

PROC class() OF whitenoise IS 'whitenoise'

PROC process() OF whitenoise
	SUPER self.process()
	self.output(ID_MAIN, frnd())
ENDPROC

/*--------------------------------------------------------------------------+
| END: whitenoise.e                                                         |
+==========================================================================*/
