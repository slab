/*==========================================================================+
| feedback.e                                                                |
| Effect class "feedback"                                                   |
| Allow feedback loops                                                      |
|                                                                           |
| Inherits from in1out1, adding to methods process and clear, and setting   |
| issource to TRUE and isready to FALSE                                     |
|                                                                           |
| isready() is FALSE, so that process is not called when input is recieved  |
| issource() is TRUE, so process is called by kernel in run                 |
| clear() stores data for next sample                                       |
|                                                                           |
| Due to feedback, input cannot reach the feedback object until after its   |
| process has been called.                                                  |
|                                                                           |
| --object--feedback--\     object needs data from feedback to send input   |
|     \---------------/     to feedback, so nothing happens until run calls |
|                           feedback                                        |
|                                                                           |
+--------------------------------------------------------------------------*/

OPT MODULE

MODULE '*defs', '*in1out1'

/*-------------------------------------------------------------------------*/

EXPORT OBJECT feedback OF in1out1
PRIVATE
	last       : LONG     -> previous input
	checking   : LONG     -> prevent loops in check
	processing : LONG     -> prevent loops in process
ENDOBJECT

PROC class()    OF feedback IS 'feedback'
PROC issource() OF feedback IS TRUE
PROC isready()  OF feedback IS FALSE

/*-------------------------------------------------------------------------*/

PROC new(list, name) OF feedback
	SUPER self.new(list, name)
	self.last := 0.0
	self.checking := FALSE
	self.processing := FALSE
ENDPROC

/*-------------------------------------------------------------------------*/

PROC clear() OF feedback
	self.last := self._main()
	SUPER self.clear()
ENDPROC

/*-------------------------------------------------------------------------*/

PROC reset() OF feedback
	SUPER self.reset()
	self.last := 0.0
ENDPROC

/*-------------------------------------------------------------------------*/

PROC process() OF feedback
	IF self.processing = FALSE  -> prevent loop
		self.processing := TRUE
		SUPER self.process()
		self.output(ID_MAIN, self.last)
		self.processing := FALSE
	ENDIF
ENDPROC

/*-------------------------------------------------------------------------*/

PROC check() OF feedback
	IF self.checking = FALSE    -> prevent loop
		self.checking := TRUE
		IF SUPER self.check() = FALSE THEN RETURN FALSE
		self.checking := FALSE
	ENDIF
ENDPROC TRUE

/*--------------------------------------------------------------------------+
| END: feedback.e                                                           |
+==========================================================================*/
