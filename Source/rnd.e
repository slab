/*==========================================================================+
| rnd.e                                                                     |
| Generate random numbers                                                   |
|                                                                           |
| initseed(newseed)     initialise the seed                                 |
| frnd()                returns a random float between -1 and 1             |
|                                                                           |
+--------------------------------------------------------------------------*/

OPT MODULE

DEF seed    -> E doesn't allow globals in modules to be initialised

EXPORT PROC initseed(newseed) IS seed := newseed

-> RndQ gives a full-range 32 bit value, not really very random though
EXPORT PROC frnd() IS (seed := RndQ(seed)) ! / (Shl(1, 31) !)

/*==========================================================================+
| END: rnd.e                                                                |
+--------------------------------------------------------------------------*/
