/*==========================================================================+
| string.e                                                                  |
| String functions                                                          |
|                                                                           |
| strcmp(str1, str2)        returns TRUE if equal (case insensitive)        |
| strncmp(str1, str2, n)    as strcmp, with number of chars to compare      |
|                                                                           |
+--------------------------------------------------------------------------*/

OPT MODULE
OPT EXPORT

/*-------------------------------------------------------------------------*/

PROC strcmp(s1 : PTR TO CHAR, s2 : PTR TO CHAR)
	IF (s1 = NIL) OR (s2 = NIL) THEN RETURN FALSE
	WHILE s1[0] = s2[0]
		IF s1[0] = 0 THEN RETURN TRUE
		INC s1
		INC s2
	ENDWHILE
ENDPROC FALSE

/*-------------------------------------------------------------------------*/

PROC strncmp(s1 : PTR TO CHAR, s2 : PTR TO CHAR, n)
	DEF i
	IF (s1 = NIL) OR (s2 = NIL) THEN RETURN FALSE
	FOR i := 0 TO n - 1
		IF s1[i] <> s2[i] THEN RETURN FALSE
	ENDFOR
ENDPROC TRUE

/*--------------------------------------------------------------------------+
| END: string.e                                                             |
+==========================================================================*/
