/*==========================================================================+
| fbdelay.e                                                                 |
| Effect class "fbdelay", delay for use in feedback loops                   |
+--------------------------------------------------------------------------*/

OPT MODULE

MODULE '*cbuffer', '*defs', '*delay', '*link'

EXPORT OBJECT fbdelay OF delay
ENDOBJECT

PROC class() OF fbdelay IS 'fbdelay'

PROC recalc() OF fbdelay
	DEF in  : PTR TO link
	SUPER self.recalc()             -> does normal delay time, and NEWs cb
	in := self.getinput(ID_MAIN)
	-> reduce delay time by 1 sample
	self.cb.setlength(! self.dt * in.rate - 1.0)
ENDPROC

/*--------------------------------------------------------------------------+
| END: fbdelay.e                                                            |
+==========================================================================*/
