/*==========================================================================+
| write.e                                                                   |
| Effect base class "write"                                                 |
| Write to a file, set by parameter "file"                                  |
|                                                                           |
| WARNING: write.reset() causes existing output file to be wiped            |
|                                                                           |
| write.startwrite()    sort out header info etc, called when file changed  |
| write.stopwrite()     clean up, called when file changed                  |
|                       both the above return success, new versions should  |
|                       call SUPER, both should handle fh being NIL         |
| write._fh()           get file handle                                     |
| write.setfile(name)   set file to name, calls stopwrite and startwrite    |
+--------------------------------------------------------------------------*/

OPT MODULE, PREPROCESS

MODULE '*defs', '*in1out0', '*string', '*value'

/*-------------------------------------------------------------------------*/

EXPORT OBJECT write OF in1out0
PRIVATE
	fname : PTR TO CHAR     -> file name
	fh    : LONG            -> file handle
ENDOBJECT

PROC class()      OF write IS 'write'
PROC _fh()        OF write IS self.fh
PROC startwrite() OF write IS TRUE      -> write header structure
PROC stopwrite()  OF write IS TRUE      -> write header data (eg length)

/*-------------------------------------------------------------------------*/

PROC setfile(fname) OF write
	self.stopwrite()
	IF self.fh THEN Close(self.fh)
	self.fname := fname
	self.fh := IF fname THEN Open(fname, NEWFILE) ELSE NIL
	self.startwrite()
ENDPROC self.fh <> NIL

/*-------------------------------------------------------------------------*/

PROC new(list, name) OF write
	SUPER self.new(list, name)
	self.setfile(NIL)
ENDPROC

/*-------------------------------------------------------------------------*/

PROC end() OF write
	self.setfile(NIL)
ENDPROC SUPER self.end()

/*-------------------------------------------------------------------------*/

PROC param2id(str) OF write
ENDPROC IF strcmp(IDS_FILE, str) THEN ID_FILE ELSE SUPER self.param2id(str)

PROC id2param(id) OF write
ENDPROC IF id = ID_FILE THEN IDS_FILE ELSE SUPER self.id2param(id)

PROC paramtype(id) OF write
ENDPROC IF id = ID_FILE THEN TYPE_STRING ELSE SUPER self.paramtype(id)

PROC set(id, data) OF write
ENDPROC IF id = ID_FILE THEN self.setfile(data) ELSE SUPER self.set(id, data)

PROC get(id) OF write
ENDPROC IF id = ID_FILE THEN self.fname ELSE SUPER self.get(id)

/*-------------------------------------------------------------------------*/

PROC reset() OF write
	SUPER self.reset()
	self.setfile(self.fname)
ENDPROC

/*--------------------------------------------------------------------------+
| END: write.e                                                              |
+==========================================================================*/
