/*==========================================================================+
| pulse.e                                                                   |
| Effect class "pulse"                                                      |
| A pulse wave oscillator, with parameter "width"                           |
+--------------------------------------------------------------------------*/

OPT MODULE, PREPROCESS
OPT EXPORT

MODULE '*defs', '*osc', '*string', '*value'

/*-------------------------------------------------------------------------*/

OBJECT pulse OF osc
PRIVATE
	width : LONG
ENDOBJECT

PROC class() OF pulse IS 'pulse'
PROC oscillator(time) OF pulse IS IF ! time > self.width THEN -1.0 ELSE 1.0

/*-------------------------------------------------------------------------*/

PROC new(list, name) OF pulse
	SUPER self.new(list, name)
	self.width := 0.5
ENDPROC

/*-------------------------------------------------------------------------*/

PROC param2id(str) OF pulse
ENDPROC IF strcmp(IDS_WIDTH, str) THEN ID_WIDTH ELSE SUPER self.param2id(str)

PROC id2param(id) OF pulse
ENDPROC IF id = ID_WIDTH THEN IDS_WIDTH ELSE SUPER self.id2param(id)

PROC paramtype(id) OF pulse
ENDPROC IF id = ID_WIDTH THEN TYPE_NUMBER ELSE SUPER self.paramtype(id)

PROC get(id) OF pulse
ENDPROC IF id = ID_WIDTH THEN self.width ELSE SUPER self.get(id)

/*-------------------------------------------------------------------------*/

PROC set(id, data) OF pulse
	SELECT id
	CASE ID_WIDTH; self.width := data
	DEFAULT;       SUPER self.set(id, data)
	ENDSELECT
ENDPROC

/*--------------------------------------------------------------------------+
| END: pulse.e                                                              |
+==========================================================================*/
