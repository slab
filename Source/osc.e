/*==========================================================================+
| osc.e                                                                     |
| Effect base class "osc"                                                   |
|                                                                           |
| Oscillator, controlled by parameters "frequency", "amplitude", "phase"    |
|                                                                           |
| osc.oscillator(time)  implemented by derived classes, this is called with |
|                       0.0 <= time < 1.0, calculated from the parameters   |
|                       and the current sample time                         |
|                                                                           |
+--------------------------------------------------------------------------*/

OPT MODULE, PREPROCESS

MODULE '*defs', '*in0out1', '*string', '*value', '*debug'

/*-------------------------------------------------------------------------*/

EXPORT OBJECT osc OF in0out1
PRIVATE
	freq   : LONG   -> frequency
	ampl   : LONG   -> amplitude
	phase  : LONG   -> phase
	time   : LONG   -> "time"
	dt     : LONG   -> "time" step
ENDOBJECT

PROC class() OF osc IS 'osc'
PROC oscillator(time) OF osc IS 0.0     -> compiler: unreferenced "time"

/*-------------------------------------------------------------------------*/

PROC new(list, name) OF osc
	SUPER self.new(list, name)
	self.rate   := DEF_RATE
	self.freq   := 440.0    -> concert pitch A
	self.ampl   := 1.0
	self.phase  := 0.0
	self.time   := 0.0
	self.setrecalc()        -> dt needs to be recalculated
ENDPROC

/*-------------------------------------------------------------------------*/

PROC param2id(str) OF osc
	IF     strcmp(IDS_FREQUENCY, str); RETURN ID_FREQUENCY
	ELSEIF strcmp(IDS_AMPLITUDE, str); RETURN ID_AMPLITUDE
	ELSEIF strcmp(IDS_PHASE,     str); RETURN ID_PHASE
	ENDIF
ENDPROC SUPER self.param2id(str)

/*-------------------------------------------------------------------------*/

PROC id2param(id) OF osc
	SELECT id
	CASE ID_FREQUENCY; RETURN IDS_FREQUENCY
	CASE ID_AMPLITUDE; RETURN IDS_AMPLITUDE
	CASE ID_PHASE;     RETURN IDS_PHASE
	ENDSELECT
ENDPROC SUPER self.id2param(id)

/*-------------------------------------------------------------------------*/

PROC paramtype(id) OF osc
	SELECT id
	CASE ID_FREQUENCY; RETURN TYPE_NUMBER
	CASE ID_AMPLITUDE; RETURN TYPE_NUMBER
	CASE ID_PHASE;     RETURN TYPE_NUMBER
	ENDSELECT
ENDPROC SUPER self.paramtype(id)

/*-------------------------------------------------------------------------*/

PROC set(id, data) OF osc
	SELECT id
	CASE ID_FREQUENCY; self.freq  := data; self.setrecalc()
	CASE ID_AMPLITUDE; self.ampl  := data
	CASE ID_PHASE;     self.phase := data
	DEFAULT;           SUPER self.set(id, data)
	ENDSELECT
ENDPROC

/*-------------------------------------------------------------------------*/

PROC get(id) OF osc
	SELECT id
	CASE ID_FREQUENCY; RETURN self.freq
	CASE ID_AMPLITUDE; RETURN self.ampl
	CASE ID_PHASE;     RETURN self.phase
	ENDSELECT
ENDPROC SUPER self.get(id)

/*-------------------------------------------------------------------------*/

PROC recalc() OF osc
	SUPER self.recalc()
	self.dt := ! self.freq / self.rate
ENDPROC

/*-------------------------------------------------------------------------*/

PROC reset() OF osc
	SUPER self.reset()
	self.time := 0.0
ENDPROC

/*-------------------------------------------------------------------------*/

PROC process() OF osc
	DEF time
	SUPER self.process()
	time      := ! self.time + self.dt              -> next t
	self.time := ! time - Ffloor(time)              -> 0 <= t < 1
	time      := ! self.time + self.phase
	time      := ! time - Ffloor(time)              -> 0 <= t < 1
	self.output(ID_MAIN, ! self.ampl * self.oscillator(time))
ENDPROC

/*--------------------------------------------------------------------------+
| END: osc.e                                                                |
+==========================================================================*/
