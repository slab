/*==========================================================================+
| value.e                                                                   |
| Value structure                                                           |
+--------------------------------------------------------------------------*/

OPT MODULE
OPT EXPORT

/*-------------------------------------------------------------------------*/

OBJECT value
PUBLIC
	type    : LONG      -> TYPE_#?
	data    : LONG
ENDOBJECT

/*-------------------------------------------------------------------------*/

OBJECT value_objpart
PUBLIC
	obj     : LONG      -> pointer to effect object (or name)
	pid     : LONG      -> effect part (input, output, param) ID (or name)
ENDOBJECT

/*-------------------------------------------------------------------------*/

ENUM TYPE_INVALID = 0,
     TYPE_NUMBER,       -> float
     TYPE_STRING,       -> character string
     TYPE_OBJPART       -> value_objpart

/*--------------------------------------------------------------------------+
| END: value.e                                                              |
+==========================================================================*/
