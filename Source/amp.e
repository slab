/*==========================================================================+
| amp.e                                                                     |
| Effect class "amp", amplifies input by parameter "gain"                   |
+--------------------------------------------------------------------------*/

OPT MODULE, PREPROCESS

MODULE '*defs', '*in1out1', '*string', '*value', '*debug'

/*-------------------------------------------------------------------------*/

EXPORT OBJECT amp OF in1out1
PRIVATE
	gain : LONG
ENDOBJECT

PROC class() OF amp IS 'amp'

/*-------------------------------------------------------------------------*/

PROC new(list, name) OF amp
	SUPER self.new(list, name)
	self.gain := 1.0
ENDPROC

/*-------------------------------------------------------------------------*/

PROC param2id(str) OF amp
ENDPROC IF strcmp(IDS_GAIN, str) THEN ID_GAIN ELSE SUPER self.param2id(str)

PROC id2param(id) OF amp
ENDPROC IF id = ID_GAIN THEN IDS_GAIN ELSE SUPER self.id2param(id)

PROC paramtype(id) OF amp
ENDPROC IF id = ID_GAIN THEN TYPE_NUMBER ELSE SUPER self.paramtype(id)

/*-------------------------------------------------------------------------*/

PROC set(id, data) OF amp
	SELECT id
	CASE ID_GAIN; self.gain := data
	DEFAULT;      SUPER self.set(id, data)
	ENDSELECT
ENDPROC

/*-------------------------------------------------------------------------*/

PROC get(id) OF amp
	SELECT id
	CASE ID_GAIN; RETURN self.gain
	ENDSELECT
ENDPROC SUPER self.get(id)

/*-------------------------------------------------------------------------*/

PROC process() OF amp
	SUPER self.process()
	self.output(ID_MAIN, ! self._main() * self.gain)
ENDPROC

/*--------------------------------------------------------------------------+
| END: amp.e                                                                |
+==========================================================================*/
