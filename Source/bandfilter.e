/*==========================================================================+
| bandfilter.e                                                              |
| Effect base class "bandfilter"                                            |
| Adds a parameter "bandwidth", used by bandpass and bandreject             |
+--------------------------------------------------------------------------*/

OPT MODULE, PREPROCESS

MODULE '*defs', '*filter', '*link', '*string', '*value'

/*-------------------------------------------------------------------------*/

EXPORT OBJECT bandfilter OF filter
PUBLIC
	band : LONG     -> bandwidth (Hz)
ENDOBJECT

PROC class() OF bandfilter IS 'bandfilter'

/*-------------------------------------------------------------------------*/

PROC new(list, name) OF bandfilter
	SUPER self.new(list, name)
	self.freq := 440.0
	self.band := 50.0
	self.setrecalc()
	self.reset()
ENDPROC

/*-------------------------------------------------------------------------*/

PROC param2id(str) OF bandfilter
	IF strcmp(IDS_BANDWIDTH, str) THEN RETURN ID_BANDWIDTH
ENDPROC SUPER self.param2id(str)

/*-------------------------------------------------------------------------*/

PROC id2param(id) OF bandfilter
	IF id = ID_BANDWIDTH THEN RETURN IDS_BANDWIDTH
ENDPROC SUPER self.id2param(id)

/*-------------------------------------------------------------------------*/

PROC paramtype(id) OF bandfilter
	IF id = ID_BANDWIDTH THEN RETURN TYPE_NUMBER
ENDPROC SUPER self.paramtype(id)

/*-------------------------------------------------------------------------*/

PROC set(id, data) OF bandfilter
	IF id = ID_BANDWIDTH
		IF ! data <= 0.0 THEN Throw(ERR_BAD_RANGE, id)
		self.band := data
		self.setrecalc()
	ELSE
		SUPER self.set(id, data)
	ENDIF
ENDPROC

/*-------------------------------------------------------------------------*/

PROC get(id) OF bandfilter
	IF id = ID_BANDWIDTH THEN RETURN self.band
ENDPROC SUPER self.get(id)

/*--------------------------------------------------------------------------+
| END: bandfilter.e                                                         |
+==========================================================================*/
