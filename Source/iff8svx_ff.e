/*==========================================================================+
| iff8svx_ff.e                                                              |
| IFF 8SVX file format definitions                                          |
+--------------------------------------------------------------------------*/

OPT MODULE
OPT EXPORT

/*-------------------------------------------------------------------------*/

OBJECT iff8svx_vhdr
	hioctsamples : LONG
	repeatstart  : LONG
	repeatlength : LONG
	rate         : INT      -> unsigned, but signed in E
	octaves      : CHAR
	compression  : CHAR
	volume       : LONG     -> 65536 maps to 1.0
ENDOBJECT

/*-------------------------------------------------------------------------*/

CONST MAGIC_FORM = "FORM", MAGIC_8SVX = "8SVX",
      MAGIC_VHDR = "VHDR", MAGIC_BODY = "BODY"

/*--------------------------------------------------------------------------+
| END: iff8svx_ff.e                                                         |
+==========================================================================*/
