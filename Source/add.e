/*==========================================================================+
| add.e                                                                     |
| Effect class "add"                                                        |
| Sums all inputs                                                           |
+--------------------------------------------------------------------------*/

OPT MODULE

MODULE '*defs', '*inmout1', '*debug'

EXPORT OBJECT add OF inmout1
ENDOBJECT

PROC class() OF add IS 'add'

PROC process() OF add
	DEF i, o = 0.0
	SUPER self.process()
	FOR i := 1 TO self._inputs() DO o := ! o + self._in(i)
	self.output(ID_MAIN, o)
ENDPROC

/*--------------------------------------------------------------------------+
| END: add.e                                                                |
+==========================================================================*/
