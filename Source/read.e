/*==========================================================================+
| read.e                                                                    |
| Effect base class "read"                                                  |
| Read from a file, set by parameter "file"                                 |
|                                                                           |
| read.startread()      sort out header info etc, called when file changed  |
| read.stopread()       clean up, called when file changed                  |
|                       both the above return success, new versions should  |
|                       call SUPER, both should handle fh being NIL         |
| read._fh()            get file handle                                     |
| read.setfile(name)    set file to name, calls stopread and startread      |
|                                                                           |
+--------------------------------------------------------------------------*/

OPT MODULE, PREPROCESS
OPT EXPORT

MODULE '*defs', '*in0out1', '*string', '*value'

/*-------------------------------------------------------------------------*/

OBJECT read OF in0out1
PRIVATE
	fname : PTR TO CHAR     -> file name
	fh    : LONG            -> file handle
ENDOBJECT

PROC class()     OF read IS 'read'
PROC _fh()       OF read IS self.fh
PROC startread() OF read IS TRUE      -> read header info
PROC stopread()  OF read IS TRUE      -> consistency checks ?

/*-------------------------------------------------------------------------*/

PROC setfile(fname) OF read
	self.stopread()
	IF self.fh THEN Close(self.fh)
	self.fname := fname
	self.fh := IF fname THEN Open(fname, OLDFILE) ELSE NIL
	self.startread()
ENDPROC self.fh <> NIL

/*-------------------------------------------------------------------------*/

PROC new(list, name) OF read
	SUPER self.new(list, name)
	self.setfile(NIL)
ENDPROC

/*-------------------------------------------------------------------------*/

PROC end() OF read
	self.setfile(NIL)
ENDPROC SUPER self.end()

/*-------------------------------------------------------------------------*/

PROC param2id(str) OF read
ENDPROC IF strcmp(IDS_FILE, str) THEN ID_FILE ELSE SUPER self.param2id(str)

PROC id2param(id) OF read
ENDPROC IF id = ID_FILE THEN IDS_FILE ELSE SUPER self.id2param(id)

PROC paramtype(id) OF read
ENDPROC IF id = ID_FILE THEN TYPE_STRING ELSE SUPER self.paramtype(id)

PROC set(id, data) OF read
ENDPROC IF id = ID_FILE THEN self.setfile(data) ELSE SUPER self.set(id, data)

PROC get(id) OF read
ENDPROC IF id = ID_FILE THEN self.fname ELSE SUPER self.get(id)

/*-------------------------------------------------------------------------*/

PROC reset() OF read
	SUPER self.reset()
	self.setfile(self.fname)    -> reset file to start
ENDPROC

/*--------------------------------------------------------------------------+
| END: read.e                                                               |
+==========================================================================*/
