/*==========================================================================+
| defs.e                                                                    |
| Definitions required throughout the system                                |
|                                                                           |
| ERR_#?    error IDs                                                       |
| ID_#?     IDs for inputs, outputs and parameters                          |
| IDS_#?    names for inputs, outputs and parameters                        |
| PI, PI2   mathematical constants                                          |
| DEF_RATE  default sample rate                                             |
|                                                                           |
+--------------------------------------------------------------------------*/

OPT MODULE, PREPROCESS
OPT EXPORT      -> exports all, only way to export macros

/*-------------------------------------------------------------------------*/

ENUM ERR_OK = 0,
     ERR_ALLOC_RDARGS,         ERR_BAD_ARGS,         ERR_BAD_COMMAND_LINE,
     ERR_BAD_FILE_FORMAT,      ERR_BAD_NUMBER,       ERR_BAD_PARAM_TYPE,
     ERR_BAD_RANGE,            ERR_CANT_OPEN_FILE,   ERR_CHECK,
     ERR_NAME_ALREADY_USED,    ERR_NO_SUCH_CLASS,    ERR_NO_SUCH_INPUT,
     ERR_NO_SUCH_INPUTID,      ERR_NO_SUCH_NAME,     ERR_NO_SUCH_OUTPUT,
     ERR_NO_SUCH_OUTPUTID,     ERR_NO_SUCH_PARAM,    ERR_NO_SUCH_PARAMID,
     ERR_PARAM_NOT_NUMBER,     ERR_UNKNOWN_COMMAND

/*-------------------------------------------------------------------------*/

ENUM CHECK_OK = 0,
     CHECK_BAD_INPUT_RATE, CHECK_CONTAINER,     CHECK_INPUT_NOT_CONNECTED,
     CHECK_INTERNAL_ERROR, CHECK_INVALID_PARAM, CHECK_OUTPUT_NOT_CONNECTED

/*-------------------------------------------------------------------------*/

ENUM ID_INVALID = 0,
     ID_AMPLITUDE,     ID_BANDWIDTH,     ID_DECAY,         ID_DELAY,
     ID_DEPTH,         ID_FILE,          ID_FREQUENCY,     ID_GAIN,
     ID_INPUTS,        ID_LEFT,          ID_MAIN,          ID_NORMALIZE,
     ID_OUTPUTS,       ID_PHASE,         ID_POLES,         ID_RATE,
     ID_RIGHT,         ID_SIDECHAIN,     ID_THRESHOLD,     ID_TO,
     ID_VALUE,         ID_WIDTH,         ID_ZEROS,

     ID_MULTI_IN     = $1000, ID_MULTI_OUT    = $2000,
     ID_MULTI_ZERO_R = $3000, ID_MULTI_ZERO_F = $4000,
     ID_MULTI_POLE_R = $5000, ID_MULTI_POLE_F = $6000

/*-------------------------------------------------------------------------*/

#define IDS_AMPLITUDE   'amplitude'
#define IDS_BANDWIDTH   'bandwidth'
#define IDS_DECAY       'decay'
#define IDS_DELAY       'delay'
#define IDS_DEPTH       'depth'
#define IDS_FILE        'file'
#define IDS_FREQUENCY   'frequency'
#define IDS_GAIN        'gain'
#define IDS_INPUTS      'inputs'
#define IDS_LEFT        'left'
#define IDS_MAIN        'main'
#define IDS_NORMALIZE   'normalize'
#define IDS_OUTPUTS     'outputs'
#define IDS_PHASE       'phase'
#define IDS_POLES       'poles'
#define IDS_RATE        'rate'
#define IDS_RIGHT       'right'
#define IDS_SIDECHAIN   'sidechain'
#define IDS_THRESHOLD   'threshold'
#define IDS_TO          'to'
#define IDS_VALUE       'value'
#define IDS_WIDTH       'width'
#define IDS_ZEROS       'zeros'

#define IDS_IN          'in'
#define IDS_OUT         'out'
#define IDS_POLE        'pole'
#define IDS_ZERO        'zero'

#define IDS_ON          'on'
#define IDS_OFF         'off'

/*-------------------------------------------------------------------------*/

#define PI  3.14159265
#define PI2 6.28318531

#define DEF_RATE 44100.0

/*--------------------------------------------------------------------------+
| END: defs.e                                                               |
+==========================================================================*/
