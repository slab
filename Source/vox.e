/*==========================================================================+
| vox.e                                                                     |
| Effect class "vox"                                                        |
|                                                                           |
| Sends out input only once it has exceeded parameter "threshold"           |
|                                                                           |
| WARNING: effects linked (even indirectly) to one vox can have no inputs   |
| not from that vox without unpredictable results (removing a certain vox   |
| should cause the network of effects to fall into two unconnected parts).  |
|                                                                           |
+--------------------------------------------------------------------------*/

OPT MODULE, PREPROCESS

MODULE '*defs', '*in1out1', '*string', '*value'

/*-------------------------------------------------------------------------*/

EXPORT OBJECT vox OF in1out1
PRIVATE
	threshold : LONG    -> cutoff level
	max       : LONG    -> current maximum attained (threshold may change)
ENDOBJECT

PROC class() OF vox IS 'vox'

/*-------------------------------------------------------------------------*/

PROC new(list, name) OF vox
	SUPER self.new(list, name)
	self.threshold := 0.0001
	self.reset()
ENDPROC

/*-------------------------------------------------------------------------*/

PROC param2id(str) OF vox
	IF strcmp(IDS_THRESHOLD, str); RETURN ID_THRESHOLD
	ENDIF
ENDPROC SUPER self.param2id(str)

/*-------------------------------------------------------------------------*/

PROC id2param(id) OF vox
	SELECT id
	CASE ID_THRESHOLD; RETURN IDS_THRESHOLD
	ENDSELECT
ENDPROC SUPER self.id2param(id)

/*-------------------------------------------------------------------------*/

PROC paramtype(id) OF vox
	SELECT id
	CASE ID_THRESHOLD; RETURN TYPE_NUMBER
	ENDSELECT
ENDPROC SUPER self.paramtype(id)

/*-------------------------------------------------------------------------*/

PROC set(id, data) OF vox
	SELECT id
	CASE ID_THRESHOLD; self.threshold := data
	DEFAULT;           SUPER self.set(id, data)
	ENDSELECT
ENDPROC

/*-------------------------------------------------------------------------*/

PROC get(id) OF vox
	SELECT id
	CASE ID_THRESHOLD; RETURN self.threshold
	ENDSELECT
ENDPROC SUPER self.get(id)

/*-------------------------------------------------------------------------*/

PROC reset() OF vox
	SUPER self.reset()
	self.max := 0.0
ENDPROC

/*-------------------------------------------------------------------------*/

PROC process() OF vox
	SUPER self.process()
	IF ! Fabs(self._main()) > self.max THEN self.max := Fabs(self._main())
	IF ! self.max > self.threshold THEN self.output(ID_MAIN, self._main())
ENDPROC

/*--------------------------------------------------------------------------+
| END: vox.e                                                                |
+==========================================================================*/
