/*==========================================================================+
| toparam.e                                                                 |
| Effect class "toparam"                                                    |
| Set parameters according to sample data                                   |
|                                                                           |
| isready() is FALSE, so that process is not called when input is recieved  |
| issource() is TRUE, so process is called by kernel in run                 |
|                                                                           |
| The run command moves all toparam objects to the end of the list, so that |
| their input will have arrived by the time process is called.  For this    |
| reason class() should not be replaced in any derived classes.             |
|                                                                           |
+--------------------------------------------------------------------------*/

OPT MODULE, PREPROCESS

MODULE '*defs', '*effect', '*in1out0', '*string', '*value', '*debug'

/*-------------------------------------------------------------------------*/

EXPORT OBJECT toparam OF in1out0
PRIVATE
	obj  : PTR TO effect
	pid  : LONG
	last : LONG
ENDOBJECT

PROC class()    OF toparam IS 'toparam'
PROC issource() OF toparam IS TRUE
PROC isready()  OF toparam IS FALSE

/*-------------------------------------------------------------------------*/

PROC new(list, name) OF toparam
	SUPER self.new(list, name)
	self.obj  := NIL
	self.pid  := ID_INVALID
	self.last := 0.0
ENDPROC

/*-------------------------------------------------------------------------*/

PROC param2id(str) OF toparam
ENDPROC IF strcmp(IDS_TO, str) THEN ID_TO ELSE SUPER self.param2id(str)

PROC id2param(id) OF toparam
ENDPROC IF id = ID_TO THEN IDS_TO ELSE SUPER self.id2param(id)

PROC paramtype(id) OF toparam
ENDPROC IF id = ID_TO THEN TYPE_OBJPART ELSE SUPER self.paramtype(id)

/*-------------------------------------------------------------------------*/

PROC set(id, data) OF toparam
	DEF op : PTR TO value_objpart
	SELECT id
	CASE ID_TO
		op := data
		self.obj := op.obj  -> (valid) pointer, but op.pid is string
		self.pid := self.obj.param2id(op.pid)
		IF self.pid = ID_INVALID THEN Throw(ERR_NO_SUCH_PARAM, op.pid)
		IF self.obj.paramtype(self.pid) <> TYPE_NUMBER
			Throw(ERR_PARAM_NOT_NUMBER, op.pid)
		ENDIF
		self.last := 0.0
	DEFAULT; SUPER self.set(id, data)
	ENDSELECT
ENDPROC

/*-------------------------------------------------------------------------*/

PROC get(id) OF toparam
	SELECT id
	CASE ID_TO; RETURN [ self.obj, self.pid ] : value_objpart
	ENDSELECT
ENDPROC SUPER self.get(id)

/*-------------------------------------------------------------------------*/

PROC process() OF toparam
	SUPER self.process()
	IF (! self.last <> self._main()) AND self.obj
		self.obj.set(self.pid, self._main())
	ENDIF
ENDPROC

/*-------------------------------------------------------------------------*/

PROC clear() OF toparam
	self.last := self._main()
	SUPER self.clear()
ENDPROC

/*-------------------------------------------------------------------------*/

PROC reset() OF toparam
	SUPER self.reset()
	self.last := 0.0
ENDPROC

/*--------------------------------------------------------------------------+
| END: feedback.e                                                           |
+==========================================================================*/
