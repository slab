/*==========================================================================+
| list.e                                                                    |
| Low level list type, with named nodes (case insensitive, see string.e)    |
|                                                                           |
| At each end the list is terminated with a node that is part of the list   |
| structure.  See find for an example of traversing a list.                 |
|                                                                           |
| newlist(list)         prepare a list for use                              |
| add(list, node)       add a node to (the tail of) the list                |
| remove(node)          remove a node from a list (safe to remove twice)    |
| find(list, name)      find a named node, returns the node or NIL          |
|                                                                           |
+--------------------------------------------------------------------------*/

OPT MODULE, PREPROCESS
OPT EXPORT

MODULE '*string', '*debug'

/*-------------------------------------------------------------------------*/

OBJECT node
	next    : PTR TO node
	prev    : PTR TO node
	name    : PTR TO CHAR
ENDOBJECT

/*-------------------------------------------------------------------------*/

OBJECT list
	head    : node
	tail    : node
ENDOBJECT

/*-------------------------------------------------------------------------*/

PROC newlist(list : PTR TO list)
	assert(list, 'list.newlist.list')
	list.head.next := list.tail
	list.head.prev := NIL
	list.tail.next := NIL
	list.tail.prev := list.head
ENDPROC

/*-------------------------------------------------------------------------*/

PROC add(list : PTR TO list, node : PTR TO node)
	assert(list, 'list.add.list')
	assert(node, 'list.add.node')
	node.next := list.tail
	node.prev := list.tail.prev
	node.prev.next := node
	node.next.prev := node
ENDPROC

/*-------------------------------------------------------------------------*/

PROC remove(node : PTR TO node)
	assert(node, 'list.remove.node')
	IF (node.prev <> NIL) AND (node.next <> NIL)
		node.prev.next := node.next
		node.next.prev := node.prev
		node.next := NIL    -> make safe to remove() twice
		node.prev := NIL
	ENDIF
ENDPROC

/*-------------------------------------------------------------------------*/

PROC find(list : PTR TO list, name)
	DEF n : PTR TO node
	assert(list, 'list.find.list')
	n := list.head.next     -> first "real node
	WHILE n.next <> NIL     -> "fake" last node has next = NIL
		IF strcmp(n.name, name) THEN RETURN n
		n := n.next
	ENDWHILE
ENDPROC NIL

/*--------------------------------------------------------------------------+
| END: list.e                                                               |
+==========================================================================*/
