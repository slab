/*==========================================================================+
| echo.e                                                                    |
| Effect class "echo"                                                       |
|                                                                           |
| Simple echo effect, made up of one each of add, split, fbdelay, amp, and  |
| feedback.                                                                 |
|                                                                           |
| Features: input "main", output "main", parameters "decay" and "delay"     |
|                                                                           |
+--------------------------------------------------------------------------*/

OPT MODULE, PREPROCESS

MODULE '*add', '*amp', '*container', '*defs', '*fbdelay', '*feedback',
       '*split', '*string', '*link'

/*-------------------------------------------------------------------------*/

EXPORT OBJECT echo OF container
PRIVATE
	add      : PTR TO add         -> for delegation purposes only
	split    : PTR TO split
	fbdelay  : PTR TO fbdelay
	amp      : PTR TO amp
ENDOBJECT

PROC class()    OF echo IS 'echo'
PROC issource() OF echo IS TRUE     -> contains source objects

/*-------------------------------------------------------------------------*/

PROC new(list, name) OF echo
	DEF fb : PTR TO feedback
	SUPER self.new(list, name)
	-> create parts
	NEW self.add    .new(self.list, 'echo_add')
	NEW self.split  .new(self.list, 'echo_split')
	NEW self.fbdelay.new(self.list, 'echo_fbdelay')
	NEW self.amp    .new(self.list, 'echo_amp')
	NEW fb          .new(self.list, 'echo_feedback')
	-> set parameters
	self.add    .set(ID_INPUTS,  2.0)
	self.split  .set(ID_OUTPUTS, 2.0)
	self.fbdelay.set(ID_DELAY,   0.1)
	self.amp    .set(ID_GAIN,    0.5)
	-> link
	link(self.add,     ID_MAIN,          self.split,   ID_MAIN)
	link(self.split,   ID_MULTI_OUT + 2, self.fbdelay, ID_MAIN)
	link(self.fbdelay, ID_MAIN,          self.amp,     ID_MAIN)
	link(self.amp,     ID_MAIN,          fb,           ID_MAIN)
	link(fb,           ID_MAIN,          self.add,     ID_MULTI_IN + 2)
ENDPROC

/*-------------------------------------------------------------------------*/

PROC input2id(str) OF echo
ENDPROC IF strcmp(IDS_MAIN, str) THEN ID_MAIN ELSE SUPER self.input2id(str)

PROC output2id(str) OF echo
ENDPROC IF strcmp(IDS_MAIN, str) THEN ID_MAIN ELSE SUPER self.output2id(str)

PROC id2input(id) OF echo
ENDPROC IF id = ID_MAIN THEN IDS_MAIN ELSE SUPER self.id2input(id)

PROC id2output(id) OF echo
ENDPROC IF id = ID_MAIN THEN IDS_MAIN ELSE SUPER self.id2output(id)

/*-------------------------------------------------------------------------*/

PROC param2id(str) OF echo
	IF     strcmp(IDS_DECAY, str); RETURN ID_DECAY
	ELSEIF strcmp(IDS_DELAY, str); RETURN ID_DELAY
	ENDIF
ENDPROC SUPER self.param2id(str)

/*-------------------------------------------------------------------------*/

PROC id2param(id) OF echo
	SELECT id
	CASE ID_DECAY; RETURN self.amp.id2param(ID_GAIN)
	CASE ID_DELAY; RETURN self.fbdelay.id2param(ID_DELAY)
	ENDSELECT
ENDPROC SUPER self.id2param(id)

/*-------------------------------------------------------------------------*/

PROC paramtype(id) OF echo
	SELECT id
	CASE ID_DECAY; RETURN self.amp.paramtype(ID_GAIN)
	CASE ID_DELAY; RETURN self.fbdelay.paramtype(ID_DELAY)
	ENDSELECT
ENDPROC SUPER self.paramtype(id)

/*-------------------------------------------------------------------------*/

PROC set(id, data) OF echo
	SELECT id
	CASE ID_DECAY; self.amp.set(ID_GAIN, data)
	CASE ID_DELAY; self.fbdelay.set(ID_DELAY, data)
	DEFAULT;       SUPER self.set(id, data)
	ENDSELECT
ENDPROC

/*-------------------------------------------------------------------------*/

PROC get(id) OF echo
	SELECT id
	CASE ID_DECAY; RETURN self.amp.get(ID_GAIN)
	CASE ID_DELAY; RETURN self.fbdelay.get(ID_DELAY)
	ENDSELECT
ENDPROC SUPER self.get(id)

/*-------------------------------------------------------------------------*/

PROC getinput(id) OF echo
	SELECT id
	CASE ID_MAIN; RETURN self.add.getinput(ID_MULTI_IN + 1)
	ENDSELECT
ENDPROC SUPER self.getinput(id)

/*-------------------------------------------------------------------------*/

PROC setoutput(id, link) OF echo
	SELECT id
	CASE ID_MAIN; RETURN self.split.setoutput(ID_MULTI_OUT + 1, link)
	ENDSELECT
ENDPROC SUPER self.setoutput(id, link)

/*--------------------------------------------------------------------------+
| END: echo.e                                                               |
+==========================================================================*/
