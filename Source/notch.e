/*==========================================================================+
| notch.e                                                                   |
| Effect class "notch"                                                      |
|                                                                           |
| Simple notch filter, using a zfilter with 2 poles and 2 zeros.  The zeros |
| are at the "frequency" specified with radius 1, the poles are at radius   |
| "depth".  The closer "depth" is to 1, the narrower the notch.             |
|                                                                           |
| Features: input "main", output "main", parameters "depth" and "frequency" |
|                                                                           |
+--------------------------------------------------------------------------*/

OPT MODULE, PREPROCESS

MODULE '*container', '*defs', '*string', '*zfilter'

/*-------------------------------------------------------------------------*/

EXPORT OBJECT notch OF container
PRIVATE
	filter : PTR TO zfilter         -> for delegation purposes only
ENDOBJECT

PROC class() OF notch IS 'notch'

/*-------------------------------------------------------------------------*/

PROC new(list, name) OF notch
	SUPER self.new(list, name)
	-> create parts
	NEW self.filter.new(self.list, 'notch_zfilter')
	-> set parameters
	self.filter.set(ID_POLES, 2.0)
	self.filter.set(ID_ZEROS, 2.0)
	self.filter.set(ID_MULTI_ZERO_R + 1, 1.0)
	self.filter.set(ID_MULTI_ZERO_R + 2, 1.0)
	self.set(ID_DEPTH, 0.9)
	self.set(ID_FREQUENCY, 440.0)
ENDPROC

/*-------------------------------------------------------------------------*/

PROC input2id(str) OF notch
ENDPROC IF strcmp(IDS_MAIN, str) THEN ID_MAIN ELSE SUPER self.input2id(str)

PROC output2id(str) OF notch
ENDPROC IF strcmp(IDS_MAIN, str) THEN ID_MAIN ELSE SUPER self.output2id(str)

PROC id2input(id) OF notch
ENDPROC IF id = ID_MAIN THEN IDS_MAIN ELSE SUPER self.id2input(id)

PROC id2output(id) OF notch
ENDPROC IF id = ID_MAIN THEN IDS_MAIN ELSE SUPER self.id2output(id)

/*-------------------------------------------------------------------------*/

PROC param2id(str) OF notch
	IF     strcmp(IDS_DEPTH,     str); RETURN ID_DEPTH
	ELSEIF strcmp(IDS_FREQUENCY, str); RETURN ID_FREQUENCY
	ENDIF
ENDPROC SUPER self.param2id(str)

/*-------------------------------------------------------------------------*/

PROC id2param(id) OF notch
	SELECT id
	CASE ID_DEPTH;     RETURN IDS_DEPTH
	CASE ID_FREQUENCY; RETURN IDS_FREQUENCY
	ENDSELECT
ENDPROC SUPER self.id2param(id)

/*-------------------------------------------------------------------------*/

PROC paramtype(id) OF notch
	SELECT id
	CASE ID_DEPTH;     RETURN self.filter.paramtype(ID_MULTI_POLE_R + 1)
	CASE ID_FREQUENCY; RETURN self.filter.paramtype(ID_MULTI_POLE_F + 1)
	ENDSELECT
ENDPROC SUPER self.paramtype(id)

/*-------------------------------------------------------------------------*/

PROC set(id, data) OF notch
	SELECT id
	CASE ID_DEPTH
		self.filter.set(ID_MULTI_POLE_R + 1, data)
		self.filter.set(ID_MULTI_POLE_R + 2, data)
		self.setrecalc()
	CASE ID_FREQUENCY
		self.filter.set(ID_MULTI_POLE_F + 1, data)
		self.filter.set(ID_MULTI_POLE_F + 2, ! -data)
		self.filter.set(ID_MULTI_ZERO_F + 1, data)
		self.filter.set(ID_MULTI_ZERO_F + 2, ! -data)
		self.setrecalc()
	DEFAULT; SUPER self.set(id, data)
	ENDSELECT
ENDPROC

/*-------------------------------------------------------------------------*/

PROC get(id) OF notch
	SELECT id
	CASE ID_DEPTH;     RETURN self.filter.get(ID_MULTI_POLE_R + 1)
	CASE ID_FREQUENCY; RETURN self.filter.get(ID_MULTI_POLE_F + 1)
	ENDSELECT
ENDPROC SUPER self.get(id)

/*-------------------------------------------------------------------------*/

PROC getinput(id) OF notch
	SELECT id
	CASE ID_MAIN; RETURN self.filter.getinput(id)
	ENDSELECT
ENDPROC SUPER self.getinput(id)

/*-------------------------------------------------------------------------*/

PROC setoutput(id, link) OF notch
	SELECT id
	CASE ID_MAIN; RETURN self.filter.setoutput(id, link)
	ENDSELECT
ENDPROC SUPER self.setoutput(id, link)

/*--------------------------------------------------------------------------+
| END: notch.e                                                              |
+==========================================================================*/
