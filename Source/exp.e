/*==========================================================================+
| exp.e                                                                     |
| Effect class "exp", exponential oscillator, varies between 0 and 1        |
+--------------------------------------------------------------------------*/

OPT MODULE

MODULE '*osc'

EXPORT OBJECT exp OF osc
ENDOBJECT

PROC oscillator(time) OF exp IS ! Fexp(! Flog(2.0) * time) - 1.0

PROC class() OF exp IS 'exp'

/*--------------------------------------------------------------------------+
| END: exp.e                                                                |
+==========================================================================*/
