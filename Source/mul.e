/*==========================================================================+
| mul.e                                                                     |
| Effect class "mul", multiplies all inputs                                 |
+--------------------------------------------------------------------------*/

OPT MODULE

MODULE '*defs', '*inmout1', '*debug'

EXPORT OBJECT mul OF inmout1
ENDOBJECT

PROC class() OF mul IS 'mul'

PROC process() OF mul
	DEF i, o = 0.0
	SUPER self.process()
	FOR i := 1 TO self._inputs() DO o := ! o * self._in(i)
	self.output(ID_MAIN, o)
ENDPROC

/*--------------------------------------------------------------------------+
| END: mul.e                                                                |
+==========================================================================*/
