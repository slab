/*==========================================================================+
| testrnd.e                                                                 |
| Test random numbers                                                       |
+--------------------------------------------------------------------------*/

OPT REG = 5     -> use register variables

MODULE '*rnd'

PROC main()

	DEF num = 0.0, sum = 0.0, avg = 0.0, abssum = 0.0, absavg = 0.0,
	    hicnt = 0, locnt = 0, hieqcnt = 0, loeqcnt = 0, buf[16] : STRING, i

	initseed($3F44A8B3)

	FOR i := 1 TO 100000
		num := frnd()
		sum := ! sum + num
		abssum := ! abssum + Fabs(num)
		IF ! num >  1.0 THEN INC hicnt
		IF ! num < -1.0 THEN INC locnt
		IF ! num =  1.0 THEN INC hieqcnt
		IF ! num = -1.0 THEN INC loeqcnt
	ENDFOR
	avg := ! sum / 100000.0
	absavg := ! abssum / 100000.0

	PrintF('avg     = \s, expected 0.0\n', RealF(buf, avg,    8))
	PrintF('absavg  = \s, expected 0.5\n', RealF(buf, absavg, 8))
	PrintF('hicnt   = \d, should be 0\n',  hicnt)
	PrintF('locnt   = \d, should be 0\n',  locnt)
	PrintF('hieqcnt = \d, should be 0\n',  hieqcnt)
	PrintF('loeqcnt = \d, should be 0\n',  loeqcnt)

ENDPROC

/*--------------------------------------------------------------------------+
| END: testrnd.e                                                            |
+==========================================================================*/
