/*==========================================================================+
| hack.e                                                                    |
| Get an effect from the node contained in it                               |
| __node2effect(node) => effect                                             |
+--------------------------------------------------------------------------*/

OPT MODULE, PREPROCESS
OPT EXPORT

/*-------------------------------------------------------------------------*/

-> WARNING: HACK
-> This function assumes that the node is at offset 4 into the object
-> Offsets are shown by ShowModule for public fields
#define __node2effect(node) (node-4)

/*--------------------------------------------------------------------------+
| END: hack.e                                                               |
+==========================================================================*/
