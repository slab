/*==========================================================================+
| filter.e                                                                  |
| Effect base class "filter"                                                |
| Base class for lowpass, highpass, bandpass, bandreject                    |
+--------------------------------------------------------------------------*/

OPT MODULE, PREPROCESS

MODULE '*defs', '*in1out1', '*string', '*value'

/*-------------------------------------------------------------------------*/

EXPORT OBJECT filter OF in1out1
PUBLIC              -> need to be used in derived.recalc
	freq : LONG     -> frequency
	a0   : LONG     -> filter coefficients
	a1   : LONG
	a2   : LONG
	b1   : LONG
	b2   : LONG
PRIVATE
	in1  : LONG     -> previous input / output samples
	in2  : LONG     -> (not cbuffer, as small amount of data)
	out1 : LONG
	out2 : LONG
ENDOBJECT

PROC class() OF filter IS 'filter'

-> No constructor, as freq default set in derived, coefficients set
-> in recalc, previous samples set in reset

/*-------------------------------------------------------------------------*/

PROC reset() OF filter
	SUPER self.reset()
	self.in1  := 0.0
	self.in2  := 0.0
	self.out1 := 0.0
	self.out2 := 0.0
ENDPROC

/*-------------------------------------------------------------------------*/

PROC process() OF filter
	DEF in, out
	SUPER self.process()
	-> calculate
	in := self._main()
	out := ! (! self.a0 * in) + (! self.a1 * self.in1) +
	         (! self.a2 * self.in2) + (! self.b1 * self.out1) +
	         (! self.b2 * self.out2)
	-> shift to next sample
	self.out2 := self.out1
	self.out1 := out
	self.in2  := self.in1
	self.in1  := in
	-> output
	self.output(ID_MAIN, out)
ENDPROC

/*-------------------------------------------------------------------------*/

PROC param2id(str) OF filter
	IF strcmp(IDS_FREQUENCY, str); RETURN ID_FREQUENCY
	ENDIF
ENDPROC SUPER self.param2id(str)

/*-------------------------------------------------------------------------*/

PROC id2param(id) OF filter
	SELECT id
	CASE ID_FREQUENCY; RETURN IDS_FREQUENCY
	ENDSELECT
ENDPROC SUPER self.id2param(id)

/*-------------------------------------------------------------------------*/

PROC paramtype(id) OF filter
	SELECT id
	CASE ID_FREQUENCY; RETURN TYPE_NUMBER
	ENDSELECT
ENDPROC SUPER self.paramtype(id)

/*-------------------------------------------------------------------------*/

PROC set(id, data) OF filter
	SELECT id
	CASE ID_FREQUENCY
		self.freq := data
		self.setrecalc()
	DEFAULT; SUPER self.set(id, data)
	ENDSELECT
ENDPROC

/*-------------------------------------------------------------------------*/

PROC get(id) OF filter
	SELECT id
	CASE ID_FREQUENCY; RETURN self.freq
	ENDSELECT
ENDPROC SUPER self.get(id)

/*--------------------------------------------------------------------------+
| END: filter.e                                                             |
+==========================================================================*/
