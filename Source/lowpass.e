/*==========================================================================+
| lowpass.e                                                                 |
| Effect class "lowpass", low pass filter                                   |
+--------------------------------------------------------------------------*/

OPT MODULE, PREPROCESS

MODULE '*defs', '*filter', '*link'

EXPORT OBJECT lowpass OF filter
ENDOBJECT

PROC new(list, name) OF lowpass
	SUPER self.new(list, name)
	self.freq := 250.0
	self.setrecalc()
	self.reset()
ENDPROC

PROC class() OF lowpass IS 'lowpass'

PROC recalc() OF lowpass
	DEF c, in : PTR TO link
	SUPER self.recalc()
	in := self.getinput(ID_MAIN)
	c := ! 1.0 / Ftan(! PI * self.freq / in.rate)
	self.a0 := ! 1.0 / (! 1.0 + (! (! c + Fsqrt(2.0)) * c))
	self.a1 := ! 2.0 * self.a0
	self.a2 := self.a0
	self.b1 := ! self.a1 * (! c * c - 1.0)
	self.b2 := ! -self.a0 * (! 1.0 + (! (! c - Fsqrt(2.0)) * c))
ENDPROC

/*--------------------------------------------------------------------------+
| END: lowpass.e                                                            |
+==========================================================================*/
