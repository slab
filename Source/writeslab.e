/*==========================================================================+
| writeslab.e                                                               |
| Effect class "writeslab", write to a SLab sample file                     |
+--------------------------------------------------------------------------*/

OPT MODULE, PREPROCESS

MODULE '*defs', '*file', '*slab_ff', '*string', '*value', '*write', 'dos/dos'

/*-------------------------------------------------------------------------*/

EXPORT OBJECT writeslab OF write
PRIVATE
	info : slab_info    -> data
	pos  : LONG         -> current sample position (starting at 0)
ENDOBJECT

PROC class() OF writeslab IS 'writeslab'

/*-------------------------------------------------------------------------*/

PROC new(list, name) OF writeslab
	SUPER self.new(list, name)
	self.info.rate := 44100.0
	self.info.bias := 0.0
	self.info.ampl := 0.0  -> changed in process to real max
ENDPROC

/*-------------------------------------------------------------------------*/

PROC startwrite() OF writeslab
	DEF fh
	IF fh := self._fh()
		write_ulong(fh, MAGIC_SLab)
		write_ulong(fh, 0)          -> fixed in stopwrite
		write_ulong(fh, MAGIC_Info)
		write_ulong(fh, 12)
		write_ulong(fh, 0)          -> fixed in stopwrite
		write_ulong(fh, 0)          -> fixed in stopwrite
		write_ulong(fh, 0)          -> fixed in stopwrite
		write_ulong(fh, MAGIC_Data)
		write_ulong(fh, 0)          -> fixed in stopwrite
	ENDIF
	self.pos := 0
ENDPROC

/*-------------------------------------------------------------------------*/

PROC stopwrite() OF writeslab
	DEF fh
	IF fh := self._fh()
		Seek(fh,  4, OFFSET_BEGINNING)
		write_ulong(fh, 24 + (self.pos * 4))
		Seek(fh, 16, OFFSET_BEGINNING)
		write_ulong(fh, self.info.rate)
		write_ulong(fh, ! self.info.bias / (self.pos !))
		write_ulong(fh, IF !self.info.ampl=0.0 THEN 1.0 ELSE self.info.ampl)
		write_ulong(fh, MAGIC_Data)
		Seek(fh, 32, OFFSET_BEGINNING)
		write_ulong(fh, Mul(self.pos, 4))       -> normal * / is only 16 bit
	ENDIF
ENDPROC

/*-------------------------------------------------------------------------*/

PROC process() OF writeslab
	DEF smp
	smp := self._main()
	SUPER self.process()
	IF self._fh() THEN write_ulong(self._fh(), smp)
	self.info.bias := ! self.info.bias + smp
	IF ! self.info.ampl < Fabs(smp) THEN self.info.ampl := Fabs(smp)
	self.pos := self.pos + 1
ENDPROC

/*--------------------------------------------------------------------------+
| END: writeslab.e                                                          |
+==========================================================================*/
