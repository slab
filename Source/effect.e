/*==========================================================================+
| effect.e                                                                  |
| Effect base class                                                         |
|                                                                           |
| Derived classes implementing methods that are passed an id (or a string)  |
| must call SUPER if it is not recognised; the default behaviour is raising |
| an exception (ERR_NO_SUCH_#?ID, id) unless otherwise specified            |
|                                                                           |
| Derived classes may add new methods, these should not be called from      |
| outside the class (or classes derived from it)                            |
|                                                                           |
| Implementation of inputs:  see in1out0.e                                  |
| Implementation of outputs: see in0out1.e                                  |
| Implementation of params:  see constant.e                                 |
| Implementation of recalc:  see delay.e                                    |
|                                                                           |
| effect.new(list, name)        constructor, link named node into list      |
|                               derived classes must call SUPER first       |
| effect.end()                  destructor, remove node from list           |
|                               derived classes must call SUPER last        |
| effect.class()                returns the name of the class, for run time |
|                               inquiries                                   |
| effect.input2id(str)          \  get id of str (def = ID_INVALID)         |
| effect.output2id(str)         |                                           |
| effect.param2id(str)          /                                           |
| effect.id2input(id)           \  get str of id (def = NIL)                |
| effect.id2output(id)          |                                           |
| effect.id2param(id)           /                                           |
| effect.getinput(id)           get the link structure of an input          |
| effect.setoutput(id, link)    set an output link pointer                  |
| effect.set(id, value)         set a parameter, call setrecalc if the      |
|                               change requires recalculation               |
| effect.get(id)                get a parameter                             |
| effect.paramtype(id)          get the type of a parameter                 |
| effect.setrecalc()            signal that parameters have changed that    |
|                               require recalculation of internal data,     |
|                               should not be changed in derived classes    |
| effect.recalc()               recalculate internal data (def = reset      |
|                               recalc flag), derived classes must call     |
|                               SUPER first                                 |
| effect.check()                check if set up enough to run, derived      |
|                               classes should check own data before SUPER  |
|                               (for efficiency)                            |
| effect.isready()              check whether enough input to process       |
|                               (def = first), derived classes should check |
|                               own inputs before SUPER (for efficiency)    |
| effect.issource()             the effect is a source (def = FALSE)        |
| effect.clear()                clear data, ready for next sample instance, |
|                               should call SUPER first                     |
| effect.reset()                clear data, ready to restart processing,    |
|                               should call SUPER first, calls clear        |
| effect.output(id, sample)     send output to destination, should call     |
|                               outputlink on internal link structure, this |
|                               method should only be called from process   |
| effect.process()              perform processing, default behaviour calls |
|                               recalc if necessary, derived classes should |
|                               call SUPER first; the kernel calls process  |
|                               for effects with issource = TRUE during run |
|                                                                           |
+--------------------------------------------------------------------------*/

OPT MODULE, PREPROCESS

MODULE '*defs', '*list', '*debug'

/*-------------------------------------------------------------------------*/

EXPORT OBJECT effect
PUBLIC
	node    : node      -> offset = 4, as class link is at 0 (see hack.e)
PRIVATE
	recalcf : LONG      -> recalc needs to be called
ENDOBJECT

/*-------------------------------------------------------------------------*/

-> Construction / destruction
PROC new(list, name)        OF effect
	self.node.name := name
	add(list, self.node)
ENDPROC
PROC end()                  OF effect IS remove(self.node)
PROC class()                OF effect IS 'effect'

-> String interface
PROC input2id (str)         OF effect IS ID_INVALID
PROC output2id(str)         OF effect IS ID_INVALID
PROC param2id (str)         OF effect IS ID_INVALID
PROC id2input (id)          OF effect IS NIL
PROC id2output(id)          OF effect IS NIL
PROC id2param (id)          OF effect IS NIL

-> Linking
PROC getinput(id)           OF effect IS Throw(ERR_NO_SUCH_INPUTID, id)
PROC setoutput(id, link)    OF effect IS Throw(ERR_NO_SUCH_OUTPUTID, id)

-> Parameters
PROC set(id, data)          OF effect IS Throw(ERR_NO_SUCH_PARAMID, id)
PROC get(id)                OF effect IS Throw(ERR_NO_SUCH_PARAMID, id)
PROC paramtype(id)          OF effect IS Throw(ERR_NO_SUCH_PARAMID, id)

-> Recalculation
PROC setrecalc()            OF effect;  self.recalcf := TRUE;  ENDPROC
PROC recalc()               OF effect;  self.recalcf := FALSE; ENDPROC

-> Processing control
PROC check()                OF effect; self.setrecalc();  ENDPROC TRUE
PROC isready()              OF effect IS TRUE
PROC issource()             OF effect IS FALSE
PROC clear()                OF effect IS EMPTY
PROC reset()                OF effect IS self.clear()

-> Processing
PROC output(id, data)       OF effect IS Throw(ERR_NO_SUCH_OUTPUTID, id)
PROC process()              OF effect
    IF self.recalcf THEN self.recalc()
ENDPROC

/*--------------------------------------------------------------------------+
| END: effect.e                                                             |
+==========================================================================*/
