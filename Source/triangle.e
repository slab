/*==========================================================================+
| triangle.e                                                                |
| Effect class "triangle", triangle oscillator                              |
+--------------------------------------------------------------------------*/

OPT MODULE

MODULE '*osc'

EXPORT OBJECT triangle OF osc
ENDOBJECT

PROC oscillator(time) OF triangle
	IF ! time < 0.25 THEN RETURN ! 4.0 * time
	IF ! time < 0.75 THEN RETURN ! 4.0 * (! 0.5 - time)
	IF ! time < 1.00 THEN RETURN ! 4.0 * (! time - 1.0)
ENDPROC 0.0

PROC class() OF triangle IS 'triangle'

/*--------------------------------------------------------------------------+
| END: triangle.e                                                           |
+==========================================================================*/
