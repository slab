/*==========================================================================+
| scale.e                                                                   |
| Effect class "scale", scales input to fit a new range                     |
+--------------------------------------------------------------------------*/

OPT MODULE

MODULE '*defs', '*in1out1'

EXPORT OBJECT scale OF in1out1
ENDOBJECT

PROC class() OF scale IS 'scale'

PROC process() OF scale
	SUPER self.process()
	self.output(ID_MAIN, scale(0.0, 1.0, 20.0, 2000.0, self._main()))
ENDPROC

PROC scale(min0, max0, min1, max1, x)
	DEF shift, scale
	shift := ! max1 - max0
	scale := ! (! max1 - min1) / (! max0 - min0)
ENDPROC ! (! x + shift) * scale

/*--------------------------------------------------------------------------+
| END: scale.e                                                              |
+==========================================================================*/
