/*==========================================================================+
| container.e                                                               |
| Effect base class "container"                                             |
|                                                                           |
| Base class for effects built up from others.  An internal list is kept of |
| the effect objects making up the compound effect.  No inputs, outputs or  |
| parameters are defined.  Objects are the responsibility of the container, |
| which handles most functions.  Derived classes add objects and delegate   |
| inputs, outputs and parameters.                                           |
|                                                                           |
| See echo.e and notch.e for examples.                                      |
|                                                                           |
| Members are added using the public list as an argument for new.           |
|                                                                           |
| Note: the fix in kernel to move toparam objects to the end of the list is |
| not present, so they must be added last.                                  |
|                                                                           |
| container.new(list, name)     initialises the internal list of members    |
| container.end()               ends all members                            |
| container.clear()             clears all members                          |
| container.reset()             resets all members                          |
| container.process()           called if issource is set, calls process    |
|                           	for all members that have issource set      |
| container.check()             checks all members that have issource set   |
|                                                                           |
+--------------------------------------------------------------------------*/

OPT MODULE, PREPROCESS

MODULE '*defs', '*effect', '*hack', '*list'

/*-------------------------------------------------------------------------*/

EXPORT OBJECT container OF effect
PUBLIC
	list : list     -> list of member objects (needed by derived classes)
ENDOBJECT

PROC class() OF container IS 'container'

/*-------------------------------------------------------------------------*/

PROC new(list, name) OF container
	SUPER self.new(list, name)
	newlist(self.list)
ENDPROC

/*-------------------------------------------------------------------------*/

PROC end() OF container
	DEF n : PTR TO node, fx : PTR TO effect, t
	n := self.list.head.next
	WHILE (t := n.next) <> NIL
		fx := __node2effect(n)
		END fx
		n := t
	ENDWHILE
	SUPER self.end()
ENDPROC

/*-------------------------------------------------------------------------*/

PROC process() OF container
	DEF n : PTR TO node, fx : PTR TO effect
	SUPER self.process()
	n := self.list.head.next
	WHILE n.next <> NIL
		fx := __node2effect(n)
		IF fx.issource() THEN fx.process()
		n := n.next
	ENDWHILE
ENDPROC

/*-------------------------------------------------------------------------*/

PROC clear() OF container
	DEF n : PTR TO node, fx : PTR TO effect
	SUPER self.clear()
	n := self.list.head.next
	WHILE n.next <> NIL
		fx := __node2effect(n)
		fx.clear()
		n := n.next
	ENDWHILE
ENDPROC

/*-------------------------------------------------------------------------*/

PROC reset() OF container
	DEF n : PTR TO node, fx : PTR TO effect
	SUPER self.reset()
	n := self.list.head.next
	WHILE n.next <> NIL
		fx := __node2effect(n)
		fx.reset()
		n := n.next
	ENDWHILE
ENDPROC

/*-------------------------------------------------------------------------*/

PROC check() OF container HANDLE    -> internal objects should not be visible
	DEF n : PTR TO node, fx : PTR TO effect, ok
	IF SUPER self.check() = FALSE THEN RETURN FALSE
	ok := TRUE
	n := self.list.head.next
	WHILE n.next <> NIL
		fx := __node2effect(n)
		IF fx.issource() THEN ok := ok AND fx.check()
		n := n.next
	ENDWHILE
EXCEPT          -> catch errors, pass on with extra information
	IF exception = ERR_CHECK
		Throw(ERR_CHECK, [ CHECK_CONTAINER, self, exceptioninfo ])
	ENDIF
	ReThrow()   -> throw exception if it is <> 0
ENDPROC ok

/*--------------------------------------------------------------------------+
| END: echo.e                                                               |
+==========================================================================*/
