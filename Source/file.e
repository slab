/*==========================================================================+
| file.e                                                                    |
| Functions for reading / writing / converting integers                     |
|                                                                           |
| Types: ( s | u ) ( byte | word | long ) ( | _x ), where                   |
| - s = signed (2's complement), u = unsigned                               |
| - byte = 8 bits, word = 16 bits, long = 32 bits                           |
| - (def) = msb first (eg $01234567), _x = lsb first (eg $67452301)         |
| NB: all longs in E are signed, so ulongs are really slongs                |
|                                                                           |
| read / write return success, others may raise ("file", fh) on errors      |
|                                                                           |
+--------------------------------------------------------------------------*/

OPT MODULE

/*-------------------------------------------------------------------------*/

-> Read functions
EXPORT PROC read_sbyte(fh)   IS sbyte(      _read(fh, 1) )
EXPORT PROC read_ubyte(fh)   IS             _read(fh, 1)
EXPORT PROC read_sword(fh)   IS sword(      _read(fh, 2) )
EXPORT PROC read_uword(fh)   IS             _read(fh, 2)
EXPORT PROC read_slong(fh)   IS             _read(fh, 4)
EXPORT PROC read_ulong(fh)   IS             _read(fh, 4)
EXPORT PROC read_sword_x(fh) IS sword(xword(_read(fh, 2)))
EXPORT PROC read_uword_x(fh) IS       xword(_read(fh, 2))
EXPORT PROC read_slong_x(fh) IS       xlong(_read(fh, 4))
EXPORT PROC read_ulong_x(fh) IS       xlong(_read(fh, 4))

/*-------------------------------------------------------------------------*/

-> Write functions
EXPORT PROC write_sbyte(fh, data)   IS _write(fh, 1, data)
EXPORT PROC write_ubyte(fh, data)   IS _write(fh, 1, data)
EXPORT PROC write_sword(fh, data)   IS _write(fh, 2, data)
EXPORT PROC write_uword(fh, data)   IS _write(fh, 2, data)
EXPORT PROC write_slong(fh, data)   IS _write(fh, 4, data)
EXPORT PROC write_ulong(fh, data)   IS _write(fh, 4, data)
EXPORT PROC write_sword_x(fh, data) IS _write(fh, 2, xword(data))
EXPORT PROC write_uword_x(fh, data) IS _write(fh, 2, xword(data))
EXPORT PROC write_slong_x(fh, data) IS _write(fh, 4, xlong(data))
EXPORT PROC write_ulong_x(fh, data) IS _write(fh, 4, xlong(data))

/*-------------------------------------------------------------------------*/

-> Conversion functions
EXPORT PROC sbyte(ubyte) IS IF ubyte >= 128 THEN ubyte - 256 ELSE ubyte
EXPORT PROC ubyte(sbyte) IS sbyte AND $FF
EXPORT PROC sword(uword) IS IF uword >= 32768 THEN uword - 65536 ELSE uword
EXPORT PROC uword(sword) IS sword AND $FFFF
EXPORT PROC xword(word)  IS Shr(word AND $FF00, 8) OR Shl(word AND $00FF, 8)
EXPORT PROC xlong(long)  IS Shr(long AND $FF000000, 24) OR
                            Shr(long AND $00FF0000,  8) OR
                            Shl(long AND $0000FF00,  8) OR
                            Shl(long AND $000000FF, 24)

/*-------------------------------------------------------------------------*/

EXPORT PROC read (fh, buf, len)
ENDPROC Read (fh, buf, len) = len

EXPORT PROC write(fh, buf, len)
ENDPROC Write(fh, buf, len) = len

/*-------------------------------------------------------------------------*/

PROC _read (f, b); DEF d = 0      -> Read b bytes from f into lsb of d
ENDPROC IF read (f, {d} + 4 - b, b) THEN d    ELSE Throw("file", f)

PROC _write(f, b, d)              -> Write b bytes to f from lsb of d
ENDPROC IF write(f, {d} + 4 - b, b) THEN TRUE ELSE Throw("file", f)

/*--------------------------------------------------------------------------+
| END: file.e                                                               |
+==========================================================================*/
