/*==========================================================================+
| link.e                                                                    |
| Effect interconection structure and functions                             |
|                                                                           |
| For examples of usage, see in1out1.e                                      |
|                                                                           |
| newlink(link, effect, input)      initialise link, giving destination     |
| link(source, output, dest, input) link effects                            |
| clearlink(link)                   reset data and ready before next sample |
| outputlink(link, data)            calls input of the destination          |
| unlink(link)                      unlinks the input from both ends        |
|                                                                           |
+--------------------------------------------------------------------------*/

OPT MODULE, PREPROCESS
OPT EXPORT

MODULE '*effect', '*defs', '*debug'

/*-------------------------------------------------------------------------*/

OBJECT link
PUBLIC
	to      : PTR TO effect     -> owner of input
	tid     : LONG              -> input id
	from    : PTR TO effect     -> source of output
	fid     : LONG              -> output id
	data    : LONG              -> sample data
	ready   : LONG              -> input set this time
	rate    : LONG              -> link sample rate
ENDOBJECT

/*-------------------------------------------------------------------------*/

PROC newlink(link : PTR TO link, effect : PTR TO effect, input)
	assert(link, 'link.newlink')
	link.to   := effect
	link.tid  := input
	link.rate := DEF_RATE
	link.from := NIL
	link.fid  := ID_INVALID
	clearlink(link)
ENDPROC

/*-------------------------------------------------------------------------*/

PROC link(source : PTR TO effect, output, dest : PTR TO effect, input)
	DEF link = NIL : PTR TO link
	-> allow using link() to set input / output to nothing
	IF dest   THEN link := dest.getinput(input)     -> exception if invalid
	IF source THEN source.setoutput(output, link)   -> exception if invalid
	IF link                                         -> valid, change data
		link.from := source
		link.fid := output
	ENDIF
ENDPROC

/*-------------------------------------------------------------------------*/

PROC clearlink(link : PTR TO link)
	assert(link, 'link.clearlink')
	link.data  := 0.0
	link.ready := FALSE
ENDPROC

/*-------------------------------------------------------------------------*/

PROC outputlink(link : PTR TO link, data)
	assert(link,    'link.outputlink.link')
	assert(link.to, 'link.outputlink.to')
	link.data := data
	link.ready := TRUE
	IF link.to.isready() THEN link.to.process()
ENDPROC

/*-------------------------------------------------------------------------*/

PROC unlink(link : PTR TO link)
	IF link
		IF link.from <> NIL
			link.from.setoutput(link.fid, NIL)
			link.from := NIL
			link.fid := ID_INVALID
		ENDIF
		clearlink(link)
	ENDIF
ENDPROC

/*--------------------------------------------------------------------------+
| END: link.e                                                               |
+==========================================================================*/
